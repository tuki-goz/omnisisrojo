<%-- 
    Document   : newjsp7
    Created on : 07/09/2021, 05:42:12 PM
    Author     : Francisca G�mez
--%>

<%@page import="model.Variables"%>
<%@page import="model.Ordencompra"%>
<%@page import="model.Moneda"%>
<%@page import="model.Contrato"%>
<%@page import="model.Proveedor"%>
<%@page import="model.Usuario01"%>
<%@page import="model.Compra"%>
<%@page import="java.util.HashMap"%>
<%
    Compra com;
    com = (Compra) request.getAttribute("Compra");
%>
<!DOCTYPE html>

<html lang="es">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            OMINISIS
        </title>
        <link rel="icon" href="img/favicon.ico">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="demo/demo.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/estilos.css">
    </head>

    <body class="user-profile">
        <%@ page import="model.Compra, java.util.ArrayList"  %>
        <%@ page import="controller.compra"  %>

        <%
            ArrayList<Compra> compraDetalle = null;
            compraDetalle = (ArrayList<Compra>) request.getAttribute("CompraDet");
        %> 

        <div class="wrapper ">
            <div class="sidebar" data-color="red">
                <!--
                  Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
                -->
                <div class="logo">
                    <a href="#" class="simple-text logo-mini">

                    </a>
                    <a href="Principal.jsp" class="simple-text logo-normal">
                        OMNISIS
                    </a>
                </div>
                <div class="sidebar-wrapper" id="sidebar-wrapper">
                    <ul class="nav">
                        <li class="dropdown"> 
                            <a href="#">
                                <form action="compra" method="POST">      
                                    <input id="card-holder" type="text" name="buscartxt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" placeholder="Ej: 01" pattern="[0-9]{1,}" title="01" id="numero">
                                    <span>
                                        <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Buscar">
                                    </span>                         
                                </form>
                            </a>
                        </li>    
                        <li>
                            <a href="#">
                                <form action="pais" method="POST">
                                    <input type="hidden" name="buscartxt" value=" ">                             
                                    <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Activo">                              
                                </form>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <form action="pais" method="POST">
                                    <input type="hidden" name="buscartxt" value=" ">                             
                                    <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Inactivo">                              
                                </form>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <form action="pais" method="POST">
                                    <input type="hidden" name="buscartxt" value=" ">                             
                                    <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Todos">                              
                                </form>
                            </a>
                        </li>                       
                    </ul>
                </div>
            </div>
            <div class="main-panel" id="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <div class="navbar-toggle">
                                <button type="button" class="navbar-toggler">
                                    <span class="navbar-toggler-bar bar1"></span>
                                    <span class="navbar-toggler-bar bar2"></span>
                                    <span class="navbar-toggler-bar bar3"></span>
                                </button>
                            </div>
                            <a class="navbar-brand" href="#">Compra</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                        </button>



                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="panel-header panel-header-sm">
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="title">Compra</h5>
                                </div>
                                <div class="card-body">
                                    <form action="compra" method="POST">
                                        <div class="row">                                            
                                            <div class="row">
                                                <div class="form-group col-sm-12"> <%-- agregar espacio entre los labels &nbsp --%>
                                                    <div class="input-group">
                                                        <label for="card-holder">Cantidad</label>
                                                        <% if (com.getCantidad_Det() != 0) {%> 
                                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="cantidadtxt" class="form-control" aria-label="Card Holder" placeholder="Ej: 01" aria-describedby="basic-addon1" pattern="[0-9]{1,}" id="descri" value="<%= com.getCantidad_Det()%>">
                                                        <% }%>
                                                        &nbsp;&nbsp;&nbsp;<label for="card-holder">Descripcion</label>
                                                        <% if (com.getDescripcion_Det() != null) {%> 
                                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="descriptxt" class="form-control" aria-label="Card Holder" placeholder="Ej: Itemiser" aria-describedby="basic-addon1" id="cantidad" value="<%= com.getDescripcion_Det()%>">
                                                        <% }%>
                                                        <% if (com.getId_compra_Det() != 0) {%> 
                                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="hidden" name="idtxt" class="form-control" aria-label="Card Holder" placeholder="Ej: Itemiser" aria-describedby="basic-addon1" id="cantidad" value="<%= com.getId_compra_Det()%>">
                                                        <% }%>
                                                        <% if (com.getId_compra() != 0) {%> 
                                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="hidden" name="idtxt2" class="form-control" aria-label="Card Holder" placeholder="Ej: Itemiser" aria-describedby="basic-addon1" id="cantidad" value="<%= com.getId_compra()%>">
                                                        <% }%>

                                                    </div>

                                                    <div class="form-group col-sm-12"> <%-- agregar espacio entre los labels &nbsp --%>
                                                        <div class="input-group">
                                                            <label  for="selectbasic">Impuesto</label>
                                                           
                                                            <div class="col-md-4">
                                                                <select id="selectbasic" name="selectbasic" class="form-control">
                                                                    <option value="11">IVA 10</option>
                                                                    <option value="21">IVA 5</option>
                                                                    <option value="0">Exentas</option>
                                                                </select>
                                                            </div>  
                                                           
                                                            <label for="card-holder">Precio</label>
                                                            <% if (com.getPrecio_Det()!= 0) {%>
                                                            &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="prectxt" class="form-control" aria-label="Card Holder" placeholder="Ej: Scanner" aria-describedby="basic-addon1" pattern="[0-9-.]{1,}" id="precio" value="<%= com.getPrecio_Det()%>">
                                                            <% }%>
                                                        </div>
                                                    </div>
                                                    <%--BUSCADOR DEL ORDEN DE COMPRA--%>
                                                    <div class="form-group col-sm-12"> 
                                                        <label for="card-holder">Orden de Compra</label>
                                                        <select id="dropord_ord" name="dropord_ord" class="form-control" onChange="update()" >
                                                            <%
                                                                Ordencompra orden_c = new Ordencompra();
                                                                HashMap<String, String> dropo = orden_c.seleccionarOrdencompra();
                                                                for (String i : dropo.keySet()) {
                                                                    if (com.getDescrip_orden() != null) {
                                                                        if (dropo.get(i).toString().equals(com.getDescrip_orden().toString())) {
                                                                            out.println("<option value='" + i + "' selected>" + dropo.get(i) + "</option>");
                                                                        } else {
                                                                            out.println("<option value='" + i + "'>" + dropo.get(i) + "</option>");
                                                                        }
                                                                    } else {
                                                                        out.println("<option value='" + i + "'>" + dropo.get(i) + "</option>");
                                                                    }
                                                                }
                                                            %>
                                                        </select>
                                                    </div>
                                                </div> 
                                                <div>
                                                    <nav class="navbar navbar-dark navbar-expand-md navigation-clean-search">
                                                        <ul class="nav navbar-nav">
                                                            <li class="dropdown">
                                                                <input type="submit" name="accion" class="btn btn-danger btn-icon-split" value="Registrar">
                                                            </li>                                                         
                                                        </ul>
                                                    </nav>
                                                </div> 
                                                </form>
                                            </div>
                                        </div>
                                </div>
                            </div>                  
                        </div>
                    </div>
                    <!--   Core JS Files   -->
                    <script src="js/core/jquery.min.js"></script>
                    <script src="js/core/popper.min.js"></script>
                    <script src="js/core/bootstrap.min.js"></script>
                    <script src="js/plugins/perfect-scrollbar.jquery.min.js"></script>
                    <!--  Google Maps Plugin    -->
                    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
                    <!-- Chart JS -->
                    <script src="js/plugins/chartjs.min.js"></script>
                    <!--  Notifications Plugin    -->
                    <script src="js/plugins/bootstrap-notify.js"></script>
                    <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
                    <script src="js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
                    <script src="demo/demo.js"></script>
                    </body>

                    </html>