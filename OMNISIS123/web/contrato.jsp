<%-- 
    Document   : contrato
    Created on : 16/08/2021, 08:12:47 PM
    Author     : Alvaro
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.Contrato"%>
<%@page import="model.Cliente"%>
<%@page import="controller.contrato"%>
<%@page import="java.util.HashMap"%>
<%
    Contrato cont;
    cont = (Contrato) request.getAttribute("Contrato");

%>
<!DOCTYPE html>
<html>
    <head>
        <title>Contratos</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link rel="stylesheet" href="css/estiloformulario.css">
    </head>
    <style>
        .payment-form{
            padding-bottom: 50px;
            font-family: 'Montserrat', sans-serif;
        }

        .payment-form.dark{

        }
        .payment-form .content{
            box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
            background-color: white;
        }

        .payment-form .block-heading{
            padding-top: 50px;
            margin-bottom: 40px;
            text-align: center;
        }

        .payment-form .block-heading p{
            text-align: center;
            max-width: 420px;
            margin: auto;
            opacity:0.7;
        }

        .payment-form.dark .block-heading p{
            opacity:0.8;
            color: #ffffff;
        }

        .payment-form .block-heading h1,
        .payment-form .block-heading h2,
        .payment-form .block-heading h3 {
            margin-bottom:1.2rem;
            color: #ffffff;
        }

        .payment-form form{
            border-top: 2px solid #ffffff;
            box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
            background-color: #e9e9e9;
            padding: 0;
            max-width: 800px;
            margin: auto;
        }

        .payment-form .title{
            font-size: 1em;
            border-bottom: 1px solid rgba(0,0,0,0.1);
            margin-bottom: 0.8em;
            font-weight: 600;
            padding-bottom: 8px;
        }

        .payment-form .products{
            background-color: #f7fbff;
            padding: 25px;
        }

        .payment-form .products .item{
            margin-bottom:1em;
        }

        .payment-form .products .item-name{
            font-weight:600;
            font-size: 0.9em;
        }

        .payment-form .products .item-description{
            font-size:0.8em;
            opacity:0.6;
        }

        .payment-form .products .item p{
            margin-bottom:0.2em;
        }

        .payment-form .products .price{
            float: right;
            font-weight: 600;
            font-size: 0.9em;
        }

        .payment-form .products .total{
            border-top: 1px solid rgba(0, 0, 0, 0.1);
            margin-top: 10px;
            padding-top: 19px;
            font-weight: 600;
            line-height: 1;
        }

        .payment-form .card-details{
            padding: 25px 25px 15px;
        }

        .payment-form .card-details label{
            font-size: 12px;
            font-weight: 600;
            margin-bottom: 15px;
            color: #722f37;
            text-transform: uppercase;
        }

        .payment-form .card-details button{
            margin-top: 0.6em;
            padding:12px 0;
            font-weight: 600;
        }

        .payment-form .date-separator{
            margin-left: 10px;
            margin-right: 10px;
            margin-top: 5px;
        }

        @media (min-width: 576px) {
            .payment-form .title {
                font-size: 1.2em; 
            }

            .payment-form .products {
                padding: 40px; 
            }

            .payment-form .products .item-name {
                font-size: 1em; 
            }

            .payment-form .products .price {
                font-size: 1em; 
            }

            .payment-form .card-details {
                padding: 40px 40px 30px; 
            }

            .payment-form .card-details button {
                margin-top: 2em; 
            } 
        }
        /*BOTON CHECKBOX*/
        .text-small {
            font-size: 0.9rem !important;
        }

        body {
            background: linear-gradient(to left, #56ab2f, #a8e063);
        }

        .cursor-pointer {
            cursor: pointer;
        }
        body {
            background-color: #808080;
            background-image: url("data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M20 20.5V18H0v-2h20v-2H0v-2h20v-2H0V8h20V6H0V4h20V2H0V0h22v20h2V0h2v20h2V0h2v20h2V0h2v20h2V0h2v20h2v2H20v-1.5zM0 20h2v20H0V20zm4 0h2v20H4V20zm4 0h2v20H8V20zm4 0h2v20h-2V20zm4 0h2v20h-2V20zm4 4h20v2H20v-2zm0 4h20v2H20v-2zm0 4h20v2H20v-2zm0 4h20v2H20v-2z' fill='%23722f37' fill-opacity='0.4' fill-rule='evenodd'/%3E%3C/svg%3E");
        }
    </style>    

    <body>
        <main class="page payment-page">
            <section class="payment-form dark">
                <div class="container">
                    <div class="block-heading">
                        <h2>Registro del contrato</h2>
                        <p>Por favor, complete los campos</p>
                    </div>
                    <form action="contrato" method="POST">
                        <div class="card-details">
                            <h3 class="title">Datos del contrato</h3>
                            <div class="row">
                                
                                <div class="form-group col-sm-7">
                                    <label for="card-holder">Cliente</label>
                                    <select id="drop_clie" name="drop_clie" class="form-control" onChange="update()" >
                                        <%                                           
                                            Cliente clie = new Cliente();
                                            HashMap<String, String> drop_clie = clie.seleccionarCliente();
                                            for (String i : drop_clie.keySet()) {
                                                if (clie.getDesc_clie() != null) {
                                                    if (drop_clie.get(i).toString().equals(clie.getDesc_clie().toString())) {
                                                        out.println("<option value='" + i + "' selected>" + drop_clie.get(i) + "</option>");
                                                    } else {
                                                        out.println("<option value='" + i + "'>" + drop_clie.get(i) + "</option>");
                                                    }
                                                } else {
                                                    out.println("<option value='" + i + "'>" + drop_clie.get(i) + "</option>");
                                                }
                                            }
                                        %>
                                    </select>
                                </div>

                                <div class="form-group col-sm-12"> <%-- agregar espacio entre los labels &nbsp --%>                                    
                                    <label for="card-holder">Cliente de contrato</label> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp   
                                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                                    <label for="card-holder">Fiscal de contrato</label>
                                    <div class="input-group">
                                        <input id="card-holder" type="text" name="clientetxt" class="form-control" placeholder="Cliente" aria-label="Card Holder" aria-describedby="basic-addon1" value="<%= cont.getCliente_contrato()%>">
                                        <span class="input-group-addon">-</span>
                                        <input id="card-holder" type="text" name="fiscaltxt" class="form-control" placeholder="Fiscal del contrato" aria-label="Card Holder" aria-describedby="basic-addon1" value=" <%= cont.getFiscal_contrato()%>">
                                    </div>
                                </div>

                                <div class="form-group col-sm-12">
                                    <label for="card-holder">Código de contrato</label> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp   
                                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                                    <label for="card-holder">Validéz</label>
                                    <div class="input-group">
                                        <input   id="card-holder" type="text" name="codigocontratotxt" class="form-control" placeholder="Código del contrato" aria-label="Card Holder" aria-describedby="basic-addon1" value="<%= cont.getCodigo_contrato()%>">
                                        <span class="input-group-addon">-</span>
                                        <input   id="card-holder" type="text" name="valideztxt" class="form-control" placeholder="Validéz" aria-label="Card Holder" aria-describedby="basic-addon1" value="<%= cont.getValidez_contrato()%>">
                                    </div>
                                </div>                           

                                <div class="form-group col-sm-12">                               
                                    <label for="card-holder">Fecha de inicio </label> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  &nbsp
                                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  &nbsp
                                    <label for="card-holder">Fecha de final</label>
                                    <div class="input-group">
                                        <input   id="card-holder" type="text" name="fechainiciotxt" class="form-control" placeholder="Fecha de inicio" aria-label="Card Holder" aria-describedby="basic-addon1" value="<%= cont.getFecha_inicio()%>">
                                        <span class="input-group-addon">-</span>
                                        <input id="card-holder" type="text" name="fechafin" class="form-control" placeholder="Fecha de final" aria-label="Card Holder" aria-describedby="basic-addon1" value="<%= cont.getFecha_fin()%>">
                                    </div>
                                </div>

                                <div class="form-group col-sm-12">                                
                                    <label for="card-holder">Tipo de contrato</label> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  &nbsp
                                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  &nbsp

                                    <div class="input-group">
                                        <input   id="card-holder" type="text" name="tipocontratotxt" class="form-control" placeholder="Tipo de contrato" aria-label="Card Holder" aria-describedby="basic-addon1" value="<%= cont.getTipo_contrato()%>">
                                        <span class="input-group-addon">-</span>

                                    </div>
                                </div>
                                <div class="form-group col-sm-12"> <%-- agregar espacio entre los labels &nbsp --%>
                                    <div class="input-group">
                                        <label for="card-holder">Estado</label>
                                        &nbsp;&nbsp;&nbsp;
                                        <ul class="list-group">
                                            <li class="list-group-item rounded-0 d-flex align-items-center justify-content-between">
                                                <div class="custom-control custom-radio">
                                                    <input class="custom-control-input" id="customRadio1" type="radio" name="customRadio" checked="true" value="activo">
                                                    <label class="custom-control-label" for="customRadio1">
                                                        <p class="mb-0">Activo</p>
                                                    </label>
                                                </div>
                                                <label for="customRadio1"><img src="https://res.cloudinary.com/mhmd/image/upload/v1579682182/2_rqo4zs.gif" alt="" width="60"></label>
                                            </li>
                                            <li class="list-group-item d-flex align-items-center justify-content-between">
                                                <div class="custom-control custom-radio">
                                                    <input class="custom-control-input" id="customRadio2" type="radio" name="customRadio" value="inactivo">
                                                    <label class="custom-control-label" for="customRadio2">
                                                        <p class="mb-0">Inactivo</p>
                                                    </label>
                                                </div>
                                                <label for="customRadio2"><img src="https://res.cloudinary.com/mhmd/image/upload/v1579682182/1_ezgo0i.png" alt="" width="60"></label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>



                                <%--Botones--%>
                                <div>
                                    <nav class="navbar navbar-dark navbar-expand-md navigation-clean-search">
                                        <ul class="nav navbar-nav">

                                            <li class="dropdown">
                                                <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Guardar">
                                            </li>

                                            <li class="dropdown">
                                                <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Modificar">
                                            </li>

                                            <li class="dropdown">
                                                <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Estado">
                                            </li>
                                            <a class="btn btn-light action-button" role="button" href="ventas.jsp">Salir</a>
                                        </ul>
                                    </nav>
                                </div>
                                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                                <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>

                            </div>
                        </div> 
                    </form>
                </div>
            </section>
        </main>
    </body>




    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</html>
