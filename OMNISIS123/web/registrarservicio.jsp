<%-- 
    Document   : registrarservicio
    Created on : 06/08/2021, 05:21:48 PM
    Author     : Alvaro
--%>

<%@page import="model.Impuesto"%>
<%@page import="model.Usuario01"%>
<%@page import="model.Contrato"%>
<%@page import="model.Cliente"%>

<%@page import="model.Servicio"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.Tiposervicio"%>
<%@page import="controller.servicio"%>
<%@page import="controller.tipo_servicio"%>
<%
    Servicio rs;
    rs = (Servicio) request.getAttribute("Servicio");

%>
<!DOCTYPE html>
<html>
    <head>
        <title>Servicio</title>    
        <link rel="icon" href="img/favicon.ico">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link rel="stylesheet" href="css/estiloformulario.css">
    </head>
    <style>
        input:invalid { border-color: red; } input, input:valid { border-color: #ccc; }
        
        .payment-form{
            padding-bottom: 50px;
            font-family: 'Montserrat', sans-serif;
        }

        .payment-form.dark{

        }
        .payment-form .content{
            box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
            background-color: white;
        }

        .payment-form .block-heading{
            padding-top: 50px;
            margin-bottom: 40px;
            text-align: center;
        }

        .payment-form .block-heading p{
            text-align: center;
            max-width: 420px;
            margin: auto;
            opacity:0.7;
        }

        .payment-form.dark .block-heading p{
            opacity:0.8;
            color: #ffffff;
        }

        .payment-form .block-heading h1,
        .payment-form .block-heading h2,
        .payment-form .block-heading h3 {
            margin-bottom:1.2rem;
            color: #ffffff;
        }

        .payment-form form{
            border-top: 2px solid #ffffff;
            box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
            background-color: #e9e9e9;
            padding: 0;
            max-width: 1000px;
            margin: auto;
        }

        .payment-form .title{
            font-size: 1em;
            border-bottom: 1px solid rgba(0,0,0,0.1);
            margin-bottom: 0.8em;
            font-weight: 600;
            padding-bottom: 8px;
        }

        .payment-form .products{
            background-color: #f7fbff;
            padding: 25px;
        }

        .payment-form .products .item{
            margin-bottom:1em;
        }

        .payment-form .products .item-name{
            font-weight:600;
            font-size: 0.9em;
        }

        .payment-form .products .item-description{
            font-size:0.8em;
            opacity:0.6;
        }

        .payment-form .products .item p{
            margin-bottom:0.2em;
        }

        .payment-form .products .price{
            float: right;
            font-weight: 600;
            font-size: 0.9em;
        }

        .payment-form .products .total{
            border-top: 1px solid rgba(0, 0, 0, 0.1);
            margin-top: 10px;
            padding-top: 19px;
            font-weight: 600;
            line-height: 1;
        }

        .payment-form .card-details{
            padding: 25px 25px 15px;
        }

        .payment-form .card-details label{
            font-size: 12px;
            font-weight: 600;
            margin-bottom: 15px;
            color: #722f37;
            text-transform: uppercase;
        }

        .payment-form .card-details button{
            margin-top: 0.6em;
            padding:12px 0;
            font-weight: 600;
        }

        .payment-form .date-separator{
            margin-left: 10px;
            margin-right: 10px;
            margin-top: 5px;
        }

        @media (min-width: 576px) {
            .payment-form .title {
                font-size: 1.2em; 
            }

            .payment-form .products {
                padding: 40px; 
            }

            .payment-form .products .item-name {
                font-size: 1em; 
            }

            .payment-form .products .price {
                font-size: 1em; 
            }

            .payment-form .card-details {
                padding: 40px 40px 30px; 
            }

            .payment-form .card-details button {
                margin-top: 2em; 
            } 
        }
        /*BOTON CHECKBOX*/
        .text-small {
            font-size: 0.9rem !important;
        }

        body {
            background: linear-gradient(to left, #56ab2f, #a8e063);
        }

        .cursor-pointer {
            cursor: pointer;
        }
        body {
            background-color: #808080;
            background-image: url("data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M20 20.5V18H0v-2h20v-2H0v-2h20v-2H0V8h20V6H0V4h20V2H0V0h22v20h2V0h2v20h2V0h2v20h2V0h2v20h2V0h2v20h2v2H20v-1.5zM0 20h2v20H0V20zm4 0h2v20H4V20zm4 0h2v20H8V20zm4 0h2v20h-2V20zm4 0h2v20h-2V20zm4 4h20v2H20v-2zm0 4h20v2H20v-2zm0 4h20v2H20v-2zm0 4h20v2H20v-2z' fill='%23722f37' fill-opacity='0.4' fill-rule='evenodd'/%3E%3C/svg%3E");
        }
    </style>
    <script>
        var input = document.getElementById('numero');
        input.oninvalid = function (event) {
            event.target.setCustomValidity('Username should only contain lowercase letters. e.g. john');
        }
        var input = document.getElementById('descri');
        input.oninvalid = function (event) {
            event.target.setCustomValidity('Username should only contain lowercase letters. e.g. john');
        }
        var input = document.getElementById('precio');
        input.oninvalid = function (event) {
            event.target.setCustomValidity('Username should only contain lowercase letters. e.g. john');
        }
        var input = document.getElementById('iva');
        input.oninvalid = function (event) {
            event.target.setCustomValidity('Username should only contain lowercase letters. e.g. john');
        }
        var input = document.getElementById('fecha');
        input.oninvalid = function (event) {
            event.target.setCustomValidity('Username should only contain lowercase letters. e.g. john');
        }
        var input = document.getElementById('notas');
        input.oninvalid = function (event) {
            event.target.setCustomValidity('Username should only contain lowercase letters. e.g. john');
        }
    </script>
    <body>
        <main class="page payment-page">
            <section class="payment-form dark">
                <div class="container">
                    <div class="block-heading">
                        <h2>Servicio</h2>
                        <p>Por favor, complete los campos</p>
                    </div>
                    <form action="servicio" method="POST">
                        <div class="card-details">
                            <h3 class="title">Detalles del Servicio</h3>
                            <div class="row">
                                <div class="form-group col-sm-7">
                                    <label for="card-holder">Buscar</label>
                                    <input id="card-holder" type="text" name="buscartxt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" placeholder="Ej: 01" pattern="[0-9]{1,}" title="01" id="numero">                                   
                                    <span>
                                        <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Buscar">
                                    </span>
                                </div>
                                <%-- Campo para completar--%>
                                <div class="form-group col-sm-12"> <%-- agregar espacio entre los labels &nbsp --%>
                                    <div class="input-group">
                                        <label for="card-holder">Número de servicio</label>
                                        <% if (rs.getNumero_servicio_tecnico() != null) {%> 
                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="numerotxt" class="form-control" placeholder="Ej: 01" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[0-9]{1,}" title="01" id="numero" value="<%= rs.getNumero_servicio_tecnico()%>">
                                        <%  } else {%>
                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="numerotxt" class="form-control" placeholder="Ej: 01" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[0-9]{1,}" title="01" id="numero" value="">
                                        <% }%>
                                        <input id="card-holder" type="hidden" name="idtxt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" value="<%= rs.getId_servicio_tecnico()%>">
                                        &nbsp;&nbsp;&nbsp;<label for="card-holder">Descripción</label>
                                        <% if (rs.getDescripcion_servicio_tecnico() != null) {%> 
                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="descriptxt" class="form-control" placeholder="Ej: Mantenimiento de Scanner movil" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[[A-Z]{1}[a-z]{2}\D{1,}" title="Dolar" id="descri" value="<%= rs.getDescripcion_servicio_tecnico()%>">
                                        <%  } else {%>
                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="descriptxt" class="form-control" placeholder="Ej: Mantenimiento de Scanner movil" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[[A-Z]{1}[a-z]{2}\D{1,}" title="Dolar" id="descri" value="">
                                        <% }%>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12"> <%-- agregar espacio entre los labels &nbsp --%>
                                    <div class="input-group">
                                        <label for="card-holder">Tipo de servicio</label>
                                        <select id="dropts_ts" name="dropts_ts" class="form-control" onChange="update()" >

                                            <%Tiposervicio ts = new Tiposervicio();
                                                HashMap<String, String> dropts_ts = ts.seleccionarTiposervicio();
                                                for (String i : dropts_ts.keySet()) {
                                                    if (rs.getDescr_tipo_ser()!= null) {
                                                        if (dropts_ts.get(i).toString().equals(rs.getDescr_tipo_ser().toString())) {
                                                            out.println("<option value='" + i + "' selected>" + dropts_ts.get(i) + "</option>");
                                                        } else {
                                                            out.println("<option value='" + i + "'>" + dropts_ts.get(i) + "</option>");
                                                        }
                                                    } else {
                                                        out.println("<option value='" + i + "'>" + dropts_ts.get(i) + "</option>");
                                                    }

                                                }
                                            %>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12"> <%-- agregar espacio entre los labels &nbsp --%>
                                    <div class="input-group">
                                        <label for="card-holder">Precio</label>
                                        <% if (rs.getPrecio_servicio_tecnico() != 0) {%> 
                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="preciotxt" class="form-control" placeholder="Ej: 1400000000" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[0-9]{1,}" title="1400000000" id="precio" value="<%= rs.getPrecio_servicio_tecnico()%>">
                                        <%  } else {%>
                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="preciotxt" class="form-control" placeholder="Ej: 1400000000" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[0-9]{1,}" title="1400000000" id="precio" value="">
                                        <% }%>
                                        &nbsp;&nbsp;&nbsp;<label for="card-holder">IVA</label>
                                        <% if (rs.getIva_servicio_tecnico() != null) {%> 
                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="ivatxt" class="form-control" placeholder="Ej: 127272727" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[0-9]{1,}" title="127272727" id="iva" value="<%= rs.getIva_servicio_tecnico()%>">
                                        <%  } else {%>
                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="ivatxt" class="form-control" placeholder="Ej: 127272727" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[0-9]{1,}" title="127272727" id="iva" value="">
                                        <% }%>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12"> <%-- agregar espacio entre los labels &nbsp --%>
                                    <div class="input-group">
                                        <label for="card-holder">Fecha de servivio</label>
                                        <% if (rs.getFecha_servicio_tecnnico() != null) {%> 
                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="fechasertxt" class="form-control" placeholder="Ej: 01-12-2020" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[0-9-]{1,}" title="01-12-2020" id="fecha" value="<%= rs.getFecha_servicio_tecnnico()%>">
                                        <%  } else {%>
                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="fechasertxt" class="form-control" placeholder="Ej: 01-12-2020" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[0-9-]{1,}" title="01-12-2020" id="fecha" value="">
                                        <% }%>
                                    </div>
                                </div>
                                <%--BOTON CHEKBOX PARA ESTADO--%>
                                <div class="form-group col-sm-12"> <%-- agregar espacio entre los labels &nbsp --%>
                                    <div class="input-group">
                                        <label for="card-holder">Estado</label>
                                        &nbsp;&nbsp;&nbsp;
                                        <ul class="list-group">
                                            <li class="list-group-item rounded-0 d-flex align-items-center justify-content-between">
                                                <div class="custom-control custom-radio">
                                                    <input class="custom-control-input" id="customRadio1" type="radio" name="customRadio" checked="true" value="activo">
                                                    <label class="custom-control-label" for="customRadio1">
                                                        <p class="mb-0">Activo</p>
                                                    </label>
                                                </div>
                                                <label for="customRadio1"><img src="https://res.cloudinary.com/mhmd/image/upload/v1579682182/2_rqo4zs.gif" alt="" width="60"></label>
                                            </li>
                                            <li class="list-group-item d-flex align-items-center justify-content-between">
                                                <div class="custom-control custom-radio">
                                                    <input class="custom-control-input" id="customRadio2" type="radio" name="customRadio" value="inactivo">
                                                    <label class="custom-control-label" for="customRadio2">
                                                        <p class="mb-0">Inactivo</p>
                                                    </label>
                                                </div>
                                                <label for="customRadio2"><img src="https://res.cloudinary.com/mhmd/image/upload/v1579682182/1_ezgo0i.png" alt="" width="60"></label>
                                            </li>
                                        </ul>
                                        <%--BUSCADOR DEL BARRIO--%>
                                        <div class="form-group col-sm-7">
                                            <label for="card-holder">Usuario</label>
                                            <select id="dropusu_usu" name="dropusu_usu" class="form-control" onChange="update()" >
                                                <%
                                                    Usuario01 usu = new Usuario01();
                                                    HashMap<String, String> dropusu = usu.seleccionarUsuario();
                                                    for (String i : dropusu.keySet()) {
                                                        if (rs.getUsuario_carga()!= null) {
                                                            if (dropusu.get(i).toString().equals(rs.getUsuario_carga().toString())) {
                                                                out.println("<option value='" + i + "' selected>" + dropusu.get(i) + "</option>");
                                                            } else {
                                                                out.println("<option value='" + i + "'>" + dropusu.get(i) + "</option>");
                                                            }
                                                        } else {
                                                            out.println("<option value='" + i + "'>" + dropusu.get(i) + "</option>");
                                                        }
                                                    }
                                                %>
                                            </select>
                                        </div>
                                        <div class="form-group col-sm-7">
                                            <label for="card-holder">Contrato</label>
                                            <select id="drop_cont" name="drop_cont" class="form-control" onChange="update()" >
                                                <%
                                                    Contrato cont = new Contrato();
                                                    HashMap<String, String> drop_cont = cont.seleccionarContrato();
                                                    for (String i : drop_cont.keySet()) {
                                                        if (rs.getCliente_contrato()!= null) {
                                                            if (drop_cont.get(i).toString().equals(rs.getCliente_contrato().toString())) {
                                                                out.println("<option value='" + i + "' selected>" + drop_cont.get(i) + "</option>");
                                                            } else {
                                                                out.println("<option value='" + i + "'>" + drop_cont.get(i) + "</option>");
                                                            }
                                                        } else {
                                                            out.println("<option value='" + i + "'>" + drop_cont.get(i) + "</option>");
                                                        }
                                                    }
                                                %>
                                            </select>
                                        </div>
                                        <div class="form-group col-sm-7">
                                            <label for="card-holder">Impuesto</label>
                                            <select id="drop_imp" name="drop_imp" class="form-control" onChange="update()" >
                                                <%
                                                    Impuesto imp = new Impuesto();
                                                    HashMap<String, String> drop_imp = imp.seleccionarImpuesto();
                                                    for (String i : drop_imp.keySet()) {
                                                        if (rs.getDescr_impuesto()!= null) {
                                                            if (drop_imp.get(i).toString().equals(rs.getDescr_impuesto().toString())) {
                                                                out.println("<option value='" + i + "' selected>" + drop_imp.get(i) + "</option>");
                                                            } else {
                                                                out.println("<option value='" + i + "'>" + drop_imp.get(i) + "</option>");
                                                            }
                                                        } else {
                                                            out.println("<option value='" + i + "'>" + drop_imp.get(i) + "</option>");
                                                        }
                                                    }
                                                %>
                                            </select>
                                        </div>
                                        <div class="form-group col-sm-7">
                                            <label for="card-holder">Cliente</label>
                                            <select id="drop_clie" name="drop_clie" class="form-control" onChange="update()" >
                                                <%
                                                    Cliente clie = new Cliente();
                                                    HashMap<String, String> drop_clie = clie.seleccionarCliente();
                                                    for (String i : drop_clie.keySet()) {
                                                        if (rs.getCliente_contrato()!= null) {
                                                            if (drop_clie.get(i).toString().equals(rs.getCliente_contrato().toString())) {
                                                                out.println("<option value='" + i + "' selected>" + drop_clie.get(i) + "</option>");
                                                            } else {
                                                                out.println("<option value='" + i + "'>" + drop_clie.get(i) + "</option>");
                                                            }
                                                        } else {
                                                            out.println("<option value='" + i + "'>" + drop_clie.get(i) + "</option>");
                                                        }
                                                    }
                                                %>
                                            </select>
                                        </div>
                                        <div class="form-group col-sm-12"> <%-- agregar espacio entre los labels &nbsp --%>
                                            <div class="input-group">
                                                <label for="card-holder">Notas</label>
                                                <% if (rs.getNotas() != null) {%> 
                                                &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="notastxt" class="form-control" aria-label="Card Holder" placeholder="Ej: Mantenimiento de Scanner movil..." aria-describedby="basic-addon1" pattern="[a-z0-9_-.+]{3,}" title="Mantenimiento de Scanner movil..." id="notas" value="<%= rs.getNotas()%>">
                                                <%  } else {%>
                                                &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="notastxt" class="form-control" aria-label="Card Holder" placeholder="Ej: Mantenimiento de Scanner movil..." aria-describedby="basic-addon1" pattern="[a-z0-9_-.+]{3,}" title="Mantenimiento de Scanner movil..." id="notas" value="">
                                                <% }%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--BOTONES--%>
                                <div>
                                    <nav class="navbar navbar-dark navbar-expand-md navigation-clean-search">
                                        <ul class="nav navbar-nav">
                                            <li class="dropdown">
                                                <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Guardar">
                                            </li>
                                            <li class="dropdown">
                                                <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Modificar">
                                            </li>
                                            <a class="btn btn-light action-button" role="button" href="servicio.jsp">Atrás</a>
                                        </ul>
                                    </nav>
                                </div>
                                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                                <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </main>
    </body>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</html>
