<%-- 
    Document   : Principal
    Created on : 05/08/2020, 09:33:50 AM
    Author     : Alvaro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            OMINISIS - PRINCIPAL
        </title>
        <link rel="icon" href="img/favicon.ico">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="demo/demo.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/estilos.css">
    </head>

    <body >
        <div class="wrapper ">
            <div class="sidebar" data-color="red">
                <!--
                  Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
                -->
                <div class="logo">
                    <a href="#" class="simple-text logo-mini">

                    </a>
                    <a href="Principal.jsp" class="simple-text logo-normal">
                        OMNISIS
                    </a>
                </div>
                <div class="sidebar-wrapper" id="sidebar-wrapper">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#">
                                <% if (0 == 0) { %> <%--habilita el boton de acuerdo al perfil, se debe hacer por cada boton, el 1 siempre habilita y el otro mas que debe ser propio de cadad modulo, primero se debe capturar el perfil en el usuario y grabar en una clase. --%>
                                <form  action="referenciales.jsp" method="POST">
                                    <i class="now-ui-icons users_single-02"></i>
                                    <input type="submit" class="btn btn-light action-button" value="Archivos Referenciales">                               
                                </form>
                                <% }%>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#">
                                <% if (0 == 0) { %> <%--habilita el boton de acuerdo al perfil, se debe hacer por cada boton, el 1 siempre habilita y el otro mas que debe ser propio de cadad modulo, primero se debe capturar el perfil en el usuario y grabar en una clase. --%>
                                <form action="usuarios.jsp" method="POST">
                                    <i class="now-ui-icons users_single-02"></i>
                                    <input type="submit" class="btn btn-light action-button" value="Usuarios">                               
                                </form>
                                <% }%>
                            </a>
                        </li>
                        <li  class="dropdown">
                            <a href="#">
                                <% if (0 == 0) { %> <%--habilita el boton de acuerdo al perfil, se debe hacer por cada boton, el 1 siempre habilita y el otro mas que debe ser propio de cadad modulo, primero se debe capturar el perfil en el usuario y grabar en una clase. --%>
                                <form action="servicio.jsp" method="POST">
                                    <i class="now-ui-icons shopping_shop"></i>
                                    <input type="submit" class="btn btn-light action-button" value="Servicios">                                    
                                </form>
                                <% }%>
                            </a>
                        </li>
                        <li  class="dropdown">
                            <a href="#">
                                <% if (0 == 0) { %> <%--habilita el boton de acuerdo al perfil, se debe hacer por cada boton, el 1 siempre habilita y el otro mas que debe ser propio de cadad modulo, primero se debe capturar el perfil en el usuario y grabar en una clase. --%>
                                <form action="importacion.jsp" method="POST">
                                    <i class="now-ui-icons location_world"></i>
                                    <input type="submit" class="btn btn-light action-button" value="Importaciones">                                    
                                </form>
                                <% }%>
                            </a>
                        </li>
                        <li  class="dropdown">
                            <a href="#">
                                <% if (0 == 0) { %> <%--habilita el boton de acuerdo al perfil, se debe hacer por cada boton, el 1 siempre habilita y el otro mas que debe ser propio de cadad modulo, primero se debe capturar el perfil en el usuario y grabar en una clase. --%>
                                <form action="newjsp.jsp" method="POST">
                                    <i class="now-ui-icons business_money-coins"></i>
                                    <input type="submit" class="btn btn-light action-button" value="Compras">                                 
                                </form>
                                <% }%>
                            </a>
                        </li>
                        <li  class="dropdown">
                            <a href="#">
                                <% if (0 == 0) { %> <%--habilita el boton de acuerdo al perfil, se debe hacer por cada boton, el 1 siempre habilita y el otro mas que debe ser propio de cadad modulo, primero se debe capturar el perfil en el usuario y grabar en una clase. --%>
                                <form action="ventas.jsp" method="POST">
                                    <i class="now-ui-icons business_money-coins"></i>
                                    <input type="submit" class="btn btn-light action-button" value="Ventas">                                    
                                </form>
                                <% }%>
                            </a>
                        </li>                       
                        <li class="dropdown">
                            <a href="#">
                                <% if (0 == 0) { %> <%--habilita el boton de acuerdo al perfil, se debe hacer por cada boton, el 1 siempre habilita y el otro mas que debe ser propio de cadad modulo, primero se debe capturar el perfil en el usuario y grabar en una clase. --%>
                                <form action="cobros_pagos.jsp" method="POST">
                                    <i class="now-ui-icons business_money-coins"></i>
                                    <input type="submit" class="btn btn-light action-button" value="Cobranzas y Pagos">                                    
                                </form>
                                <% }%>
                            </a>
                        </li> 
                        <li class="dropdown"> 
                            <a href="#">
                                <% if (0 == 0) { %> <%--habilita el boton de acuerdo al perfil, se debe hacer por cada boton, el 1 siempre habilita y el otro mas que debe ser propio de cadad modulo, primero se debe capturar el perfil en el usuario y grabar en una clase. --%>
                                <form action="informes.jsp" method="POST">
                                    <i class="now-ui-icons travel_info"></i>
                                    <input type="submit" class="btn btn-light action-button" value="Informes">                                    
                                </form>
                                <% }%>
                            </a>
                        </li>                        
                        <li class="dropdown"> 
                            <a href="#">
                                <% if (0 == 0) { %> <%--habilita el boton de acuerdo al perfil, se debe hacer por cada boton, el 1 siempre habilita y el otro mas que debe ser propio de cadad modulo, primero se debe capturar el perfil en el usuario y grabar en una clase. --%>
                                <form action="newjsp.jsp" method="POST">
                                    <i class="now-ui-icons travel_info"></i>
                                    <input type="submit" class="btn btn-light action-button" value="VER">                                    
                                </form>
                                <% }%>
                            </a>
                        </li>                        
                    </ul>
                </div>
            </div>
            <div class="main-panel" id="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <div class="navbar-toggle">
                                <button type="button" class="navbar-toggler">
                                    <span class="navbar-toggler-bar bar1"></span>
                                    <span class="navbar-toggler-bar bar2"></span>
                                    <span class="navbar-toggler-bar bar3"></span>
                                </button>
                            </div>
                            <a class="navbar-brand" href="#pablo">Principal</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end" id="navigation">
                            <form>
                                <div class="input-group no-border">
                                    <input type="text" value="" class="form-control" placeholder="Search...">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <i class="now-ui-icons ui-1_zoom-bold"></i>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <ul class="navbar-nav">                               
                                <li class="nav-item">
                                    <a lass="dropdown-item" href="index.htm" data-toggle="modal" data-target="#logoutModal">
                                        <i class="now-ui-icons media-1_button-power"></i>
                                        <p>
                                            <span class="d-lg-none d-md-block">Salir</span>
                                        </p>
                                    </a>
                                </li>
                            </ul>                            
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="panel-header panel-header-lg">

                </div>

                <footer class="footer">
                    <div class="container mt-4">
                        <h1>Bienvenido al sistema ......<strong> ${usuar}</strong>  </h1>
                    </div>
                </footer>
            </div>
        </div>
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Seguro que quiere salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Seleccione Salir para cerrar sesion.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="index.htm">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="js/core/jquery.min.js"></script>
        <script src="js/core/popper.min.js"></script>
        <script src="js/core/bootstrap.min.js"></script>
        <script src="js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!--  Google Maps Plugin    -->
        <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
        <!-- Chart JS -->
        <script src="js/plugins/chartjs.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="js/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
        <script src="demo/demo.js"></script>
        <script>
            $(document).ready(function () {
                // Javascript method's body can be found in assets/js/demos.js
                demo.initDashboardPageCharts();

            });
        </script>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="vendor/chart.js/Chart.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="js/demo/chart-area-demo.js"></script>
        <script src="js/demo/chart-pie-demo.js"></script>
    </body>

</html>
