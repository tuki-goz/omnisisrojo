<%-- 
    Document   : newjsp6
    Created on : 07/09/2021, 04:31:51 PM
    Author     : Francisca G�mez
--%>

<%@page import="model.Estado"%>
<%@page import="model.Barrio"%>
<%@page import="model.Tipo_pago"%>
<%@page import="model.Forma_cobro"%>
<%@page import="model.Variables"%>
<%@page import="model.Ordencompra"%>
<%@page import="model.Moneda"%>
<%@page import="model.Contrato"%>
<%@page import="model.Proveedor"%>
<%@page import="model.Usuario01"%>
<%@page import="model.Compra"%>
<%@page import="java.util.HashMap"%>
<%
    Compra com;
    com = (Compra) request.getAttribute("Compra");
    String b = "";
    if (com != null) {
        b = com.getNombre_usuario();
    }
%>
<!DOCTYPE html>

<html lang="es">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            OMINISIS
        </title>
        <link rel="icon" href="img/favicon.ico">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="demo/demo.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/estilos.css">
    </head>
    <body class="user-profile">
        <%@ page import="model.Compra, java.util.ArrayList"  %>
        <%@ page import="controller.compra"  %>

        <%
            ArrayList<Compra> compraDetalle = null;
            compraDetalle = (ArrayList<Compra>) request.getAttribute("CompraDet");
        %>   
        <%
            ArrayList<Compra> compraSubTotal = null;
            compraSubTotal = (ArrayList<Compra>) request.getAttribute("CompraSub");
        %>   
        <%
            ArrayList<Compra> compraLiq = null;
            compraLiq = (ArrayList<Compra>) request.getAttribute("CompraLiq");
        %>   
        <%    Compra comp;
            comp = (Compra) request.getAttribute("Comp");
            int v_id_compra;
            int v_id_compra_Det;
            int v_cantidad_Det;
            int v_id_orden_compra;
            String v_descripcion_Det;
            Double v_iva5_Det;
            Double v_iva10_Det;
            Double v_exenta_Det;
            Double v_precio_Det;
            if (comp != null) {
                int a = comp.getCantidad_Det();
                v_id_compra = comp.getId_compra();
                v_id_compra_Det = comp.getId_compra_Det();
                v_cantidad_Det = comp.getCantidad_Det();
                v_id_orden_compra = comp.getCantidad_Det();
                v_descripcion_Det = comp.getDescripcion_Det();
                v_iva5_Det = comp.getIva5_Det();
                v_iva10_Det = comp.getIva10_Det();
                v_exenta_Det = comp.getExenta_Det();
                v_precio_Det = comp.getPrecio_Det();
                System.out.println(a);
            }

        %>

        <div class="wrapper ">
            <div class="sidebar" data-color="red">
                <!--
                  Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
                -->
                <div class="logo">
                    <a href="#" class="simple-text logo-mini">

                    </a>
                    <a href="Principal.jsp" class="simple-text logo-normal">
                        OMNISIS
                    </a>
                </div>
                <div class="sidebar-wrapper" id="sidebar-wrapper">
                    <ul class="nav">
                        <li class="dropdown"> 
                            <a href="#">
                                <form action="compra" method="POST">
                                    <input id="card-holder" type="text" name="buscartxt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" placeholder="Ej: 1" pattern="[0-9]{1,}" title="01" id="numero">
                                    <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Buscar">
                                </form>
                            </a>
                        </li>    

                        <li>
                            <a href="#">
                                <form action="compra" method="POST">
                                    <input type="hidden" name="buscartxt" value=" ">                             
                                    <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Todos">                              
                                </form>
                            </a>
                        </li>   
                         <li>    
                              <a class="btn btn-light action-button" role="button" href="newjsp.jsp">Atras</a>
                        </li>   
                    </ul>
                </div>
            </div>
            <div class="main-panel" id="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <div class="navbar-toggle">
                                <button type="button" class="navbar-toggler">
                                    <span class="navbar-toggler-bar bar1"></span>
                                    <span class="navbar-toggler-bar bar2"></span>
                                    <span class="navbar-toggler-bar bar3"></span>
                                </button>
                            </div>
                            <a class="navbar-brand" href="#">Compras</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                        </button>



                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="panel-header panel-header-sm">
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="title">Compra</h5>
                                </div>
                                <div class="card-body">
                                    <form action="compra" method="POST">
                                        <div class="row">    
                                            <div class="form-group col-sm-12">
                                                <div class="input-group">    
                                                    <input id="card-holder" type="hidden" name="idtxt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" value="<%= com.getId_compra()%>">
                                                    <label for="card-holder">Fecha compra:</label>
                                                    <% if (com.getFecha_compra() != null) {%> 
                                                    <input type="date" name="fechacompra" class="fecha" value="<%= com.getFecha_compra()%>">
                                                    <%  } else {%>
                                                    <input type="date" name="fechacompra">
                                                    <% }%>
                                                    <label for="card-holder">Condici�n:</label>
                                                    <select id="drop_tipo" name="drop_tipo" class="form-control" onChange="update()" >
                                                        <%
                                                            Tipo_pago tp = new Tipo_pago();
                                                            HashMap<String, String> driptp = tp.seleccionarTipoPago();
                                                            for (String i : driptp.keySet()) {
                                                                if (com.getDescrip_tipo_pago() != null) {
                                                                    if (driptp.get(i).toString().equals(com.getDescrip_tipo_pago().toString())) {
                                                                        out.println("<option value='" + i + "' selected>" + driptp.get(i) + "</option>");
                                                                    } else {
                                                                        out.println("<option value='" + i + "'>" + driptp.get(i) + "</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option value='" + i + "'>" + driptp.get(i) + "</option>");
                                                                }
                                                            }
                                                        %>
                                                    </select>                                                    
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-12"> <%-- agregar espacio entre los labels &nbsp --%>
                                                <div class="input-group">

                                                    <label for="card-holder">Proveedor</label>
                                                    <select id="drop_prov" name="drop_prov" class="form-control" onChange="update()" >
                                                        <%
                                                            Proveedor prov = new Proveedor();
                                                            HashMap<String, String> dropprov = prov.seleccionarProveedor();
                                                            for (String i : dropprov.keySet()) {
                                                                if (com.getDescrip_prov() != null) {
                                                                    if (dropprov.get(i).toString().equals(com.getDescrip_prov().toString())) {
                                                                        out.println("<option value='" + i + "' selected>" + dropprov.get(i) + "</option>");
                                                                    } else {
                                                                        out.println("<option value='" + i + "'>" + dropprov.get(i) + "</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option value='" + i + "'>" + dropprov.get(i) + "</option>");
                                                                }
                                                            }
                                                        %>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-12"> <%-- agregar espacio entre los labels &nbsp --%>
                                                <div class="input-group">
                                                    <label for="card-holder">C�digo:</label>
                                                    <% if (com.getCodigo() != null) {%> 
                                                    &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="codtxt" class="form-control" aria-label="Card Holder" placeholder="Ej: 270" aria-describedby="basic-addon1" id="precio" value="<%= com.getCodigo()%>"> 
                                                    <%  } else {%>
                                                    &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="codtxt" class="form-control" aria-label="Card Holder" placeholder="Ej: 270" aria-describedby="basic-addon1"  id="precio" value=""> 
                                                    <% }%>
                                                    <label for="card-holder">Contrato</label>
                                                    <% if (com.getNro_contrato() != null) {%> 
                                                    &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="nroctxt" class="form-control" aria-label="Card Holder" placeholder="Ej: CDS980" aria-describedby="basic-addon1"  id="precio" value="<%= com.getNro_contrato()%>">                                           
                                                    <%  } else {%>
                                                    &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="nroctxt" class="form-control" aria-label="Card Holder" placeholder="Ej: CDS980" aria-describedby="basic-addon1" id="precio" value="">                                           
                                                    <% }%>
                                                    <label for="card-holder">Forma de Pago:</label>
                                                    <select id="drop_form" name="drop_form" class="form-control" onChange="update()" >
                                                        <%
                                                            Forma_cobro l = new Forma_cobro();
                                                            HashMap<String, String> k = l.seleccionarFormaCobro();
                                                            for (String i : k.keySet()) {
                                                                if (com.getDescrip_forma_cobro() != null) {
                                                                    if (k.get(i).toString().equals(com.getDescrip_forma_cobro().toString())) {
                                                                        out.println("<option value='" + i + "' selected>" + k.get(i) + "</option>");
                                                                    } else {
                                                                        out.println("<option value='" + i + "'>" + k.get(i) + "</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option value='" + i + "'>" + k.get(i) + "</option>");
                                                                }
                                                            }
                                                        %>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <div class="input-group">
                                                    <label for="card-holder">Usuario</label>
                                                    <select id="drop_usu" name="drop_usu" class="form-control" onChange="update()" >
                                                        <%
                                                            Usuario01 usu = new Usuario01();
                                                            HashMap<String, String> drop = usu.seleccionarUsuario();
                                                            for (String i : drop.keySet()) {
                                                                if (com.getNombre_usuario() != null) {
                                                                    if (drop.get(i).toString().equals(com.getNombre_usuario().toString())) {
                                                                        //if (drop.get(i).toString()== b) {
                                                                        out.println("<option value='" + i + "' selected>" + drop.get(i) + "</option>");
                                                                    } else {
                                                                        out.println("<option value='" + i + "'>" + drop.get(i) + "</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option value='" + i + "'>" + drop.get(i) + "</option>");
                                                                }
                                                            }
                                                        %>
                                                    </select>
                                                    <label for="card-holder">Moneda</label>
                                                    <select id="dropmon_mon" name="dropmon_mon" class="form-control" onChange="update()" >
                                                        <%
                                                            Moneda mon = new Moneda();
                                                            HashMap<String, String> dropmon_mon = mon.seleccionarMoneda();
                                                            for (String i : dropmon_mon.keySet()) {
                                                                if (com.getDescrip_moneda() != null) {
                                                                    if (dropmon_mon.get(i).toString().equals(com.getDescrip_moneda().toString())) {
                                                                        out.println("<option value='" + i + "' selected>" + dropmon_mon.get(i) + "</option>");
                                                                    } else {
                                                                        out.println("<option value='" + i + "'>" + dropmon_mon.get(i) + "</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option value='" + i + "'>" + dropmon_mon.get(i) + "</option>");
                                                                }
                                                            }
                                                        %>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <div class="input-group">
                                                    <% if (Variables.ban1 == 0) { %>
                                                    <label for="card-holder">Orden de Compra</label>
                                                    <select id="dropord_ord" name="dropord_ord" class="form-control" onChange="update()" >
                                                        <%
                                                            Ordencompra orden_c = new Ordencompra();
                                                            HashMap<String, String> dropord_ord = orden_c.seleccionarOrdencompra();
                                                            for (String i : dropord_ord.keySet()) {
                                                                if (comp != null) {
                                                                    if (dropord_ord.get(i).toString().equals(comp.getDescrip_orden().toString())) {
                                                                        out.println("<option value='" + i + "' selected>" + dropord_ord.get(i) + "</option>");
                                                                    } else {
                                                                        out.println("<option value='" + i + "'>" + dropord_ord.get(i) + "</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option value='" + i + "'>" + dropord_ord.get(i) + "</option>");
                                                                }
                                                            }
                                                        %>
                                                    </select>
                                                    <% } %> 
                                                    <label for="card-holder">Estado</label>
                                                    <select id="drop_est" name="drop_est" class="form-control" onChange="update()" >
                                                        <%
                                                            Estado est = new Estado();
                                                            HashMap<String, String> drop_es = est.seleccionarEstado();
                                                            for (String i : drop_es.keySet()) {
                                                                if (com.getDescrip_estado() != null) {
                                                                    if (drop_es.get(i).toString().equals(com.getDescrip_estado().toString())) {
                                                                        out.println("<option value='" + i + "' selected>" + drop_es.get(i) + "</option>");
                                                                    } else {
                                                                        out.println("<option value='" + i + "'>" + drop_es.get(i) + "</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option value='" + i + "'>" + drop_es.get(i) + "</option>");
                                                                }
                                                            }
                                                        %>
                                                    </select>
                                                </div>  
                                            </div> 
                                            <% if (Variables.ocultar == 0) { %>
                                            <div class="ocultar">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <label for="card-holder">Cantidad</label>
                                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="cantidadtxt" class="form-control" aria-label="Card Holder" placeholder="Ej: 1" aria-describedby="basic-addon1" pattern="[1-9]{1,}"id="descri" value="">
                                                        <label  for="selectbasic">Impuesto</label>
                                                        <div class="col-md-4">
                                                            <select id="selectbasic" name="selectbasic" class="form-control">
                                                                <option value="11">IVA 10</option>
                                                                <option value="21">IVA 5</option>
                                                                <option value="0">Exentas</option>
                                                            </select>
                                                        </div>
                                                        <label for="card-holder">Precio</label>
                                                        &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="prectxt" class="form-control" aria-label="Card Holder" placeholder="Ej: 15000" aria-describedby="basic-addon1" pattern="[0-9-.]{1,}" id="precio" value="">
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    &nbsp;&nbsp;&nbsp;<label for="card-holder">Descripcion</label>
                                                    &nbsp;&nbsp;&nbsp;<input id="card-holder" type="text" name="descriptxt" class="form-control" aria-label="Card Holder" placeholder="Ej: Itemiser" aria-describedby="basic-addon1" pattern="[[A-Z]{1,}[a-z]{2}{1,}" id="cantidad" value="">                                               

                                                </div>
                                            </div>
                                            <% }%>
                                            &nbsp;&nbsp;&nbsp;<input id="card-holder" type="hidden" name="iva5txt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" value="">                                                 
                                            &nbsp;&nbsp;&nbsp;<input id="card-holder" type="hidden" name="iva10txt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" value="">
                                            &nbsp;&nbsp;&nbsp;<input id="card-holder" type="hidden" name="exentxt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" value="">
                                            &nbsp;&nbsp;&nbsp;<input id="card-holder" type="hidden" name="totaltxt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" value="">
                                            &nbsp;&nbsp;&nbsp;<input id="card-holder" type="hidden" name="subtxt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" value="">
                                            &nbsp;&nbsp;&nbsp;<input id="card-holder" type="hidden" name="liq5txt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" value="">
                                            &nbsp;&nbsp;&nbsp;<input id="card-holder" type="hidden" name="liq10txt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" value="">
                                            <div class="container-fluid margin">
                                                <input type="hidden" name="detallecompra" value=" ">
                                                <% if (Variables.ban1 == 1) { %>
                                                <input type="submit" name="accion"  class="btn btn-danger btn-icon-split" value="Modificar">
                                                <% } else { %>
                                                <input type="submit" name="accion" target="_blank" class="btn btn-danger btn-icon-split" value="Ingresar">                                             
                                                <% } %>
                                            </div>
                                            <div class="container-fluid margin">

                                            </div>
                                            <table class="table table-striped custab">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Cantidad:</th>
                                                        <th class="text-center">Descripci�n:</th>
                                                        <th class="text-center">Precio Uni:</th>
                                                        <th class="text-center">Iva5:</th>
                                                        <th class="text-center">Iva10:</th>
                                                        <th class="text-center">Exenta:</th>
                                                    </tr>                                                    
                                                </thead>
                                                <%
                                                    if (compraDetalle
                                                            != null) {
                                                %> 
                                                <%
                                                    for (Compra comdet : compraDetalle) {
                                                %>                                            
                                                <tr>
                                                    <td class="text-center"> 
                                                        <%= comdet.getCantidad_Det()%>
                                                    </td>
                                                    <td class="text-center"> 
                                                        <%= comdet.getDescripcion_Det()%>
                                                    </td>
                                                    <td class="text-center"> 
                                                        <%= comdet.getPrecio_Det()%>
                                                    </td>
                                                    <td class="text-center"> 
                                                        <%= comdet.getIva5_Det()%>
                                                    </td>
                                                    <td class="text-center"> 
                                                        <%= comdet.getIva10_Det()%>
                                                    </td>
                                                    <td class="text-center"> 
                                                        <%= comdet.getExenta_Det()%>
                                                    </td> 
                                                    <td class="text-center"> 
                                                        <form method="POST">                                                                       
                                                            <input type="hidden" name="txt1" value="<%= comdet.getCantidad_Det()%>"> 
                                                            <input type="hidden" name="txt2" value="<%= comdet.getDescripcion_Det()%>">                                                   
                                                            <input type="hidden" name="txt6" value="<%= comdet.getPrecio_Det()%>">
                                                            <input type="hidden" name="txt3" value="<%= comdet.getIva5_Det()%>">                                                   
                                                            <input type="hidden" name="txt4" value="<%= comdet.getIva10_Det()%>">                                                   
                                                            <input type="hidden" name="txt5" value="<%= comdet.getExenta_Det()%>">                                                   
                                                            <input type="hidden" name="txt7" value="<%= comdet.getId_orden_compra()%>">    
                                                            <input type="hidden" name="idCompra" value="<%= comdet.getId_compra()%>">                                                                     
                                                            <input type="hidden" name="idCompraDet" value="<%= comdet.getId_compra_Det()%>">
                                                            <% if (Variables.ban == 1) { %>
                                                            <input type="submit" name="accion" target="_blank" class="btn btn-danger btn-icon-split" value="Cambiar">  
                                                            <% }%>
                                                        </form>
                                                    </td> 
                                                </tr> 
                                                <% } %>
                                                <% } %>
                                                <% compraDetalle.size();%>
                                            </table>
                                            <table class="table table-striped custab">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Sub-Total:</th>
                                                        <th class="text-center">Total:</th>
                                                    </tr>                                                 
                                                </thead>
                                                <%
                                                    if (compraSubTotal
                                                            != null) {
                                                %> 
                                                <%
                                                    for (Compra comdet1 : compraSubTotal) {
                                                %>                                            
                                                <tr>
                                                    <td class="text-center"> 
                                                        <%= Variables.sub_total%>
                                                    </td>
                                                    <td class="text-center"> 
                                                        <%= Variables.tota_compra%>
                                                    </td>                                                
                                                <form>                
                                                    <input type="hidden" name="txt1" value="<%= comdet1.getSub_total()%>"> 
                                                    <input type="hidden" name="txt2" value="<%= comdet1.getTotal_compra()%>">                                                    
                                                </form>
                                                </tr>
                                                <% } %>
                                                <% } %>
                                                <% compraSubTotal.clear();%>
                                            </table>
                                            <table class="table table-striped custab">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Liquidaci�n del IVA:    5%</th>
                                                        <th class="text-center">10%</th>
                                                        <th class="text-center">Total:</th>
                                                    </tr>
                                                </thead>
                                                <%
                                                    if (compraLiq
                                                            != null) {
                                                %> 
                                                <%
                                                    for (Compra liqiva : compraLiq) {
                                                %>                                            
                                                <tr>
                                                    <td class="text-center"> 
                                                        <%= liqiva.getLiq5()%>
                                                    </td>
                                                    <td class="text-center"> 
                                                        <%= liqiva.getLiq10()%>
                                                    </td>
                                                    <td class="text-center"> 
                                                        <%= liqiva.getTotaliva()%>
                                                    </td>                                                    
                                                    <td>                                                             
                                                        <input type="hidden" name="txt1" value="<%= liqiva.getLiq5()%>"> 
                                                        <input type="hidden" name="txt2" value="<%= liqiva.getLiq10()%>">  
                                                        <input type="hidden" name="txt2" value="<%= liqiva.getTotaliva()%>">  
                                                    </td>
                                                </tr>
                                                <% } %>
                                                <% } %>
                                                <% compraLiq.clear();%>
                                            </table>
                                            <div>

                                                <% if (Variables.ban2 == 0) { %>                                                       
                                                <input type="submit" name="accion" class="btn btn-danger btn-icon-split" value="Guardar">                                                        
                                                <input type="reset" class="btn btn-danger btn-icon-split" value="Limpiar">                                                        
                                                <% }%>
                                                <input type="submit" name="accion" class="btn btn-danger btn-icon-split" value="Atras">   
                                            </div> 
                                        </div>                                           
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="js/core/jquery.min.js"></script>
        <script src="js/core/popper.min.js"></script>
        <script src="js/core/bootstrap.min.js"></script>
        <script src="js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!--  Google Maps Plugin    -->
        <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
        <!-- Chart JS -->
        <script src="js/plugins/chartjs.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="js/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
        <script src="demo/demo.js"></script>
    </body>
</html>