<%-- 
    Document   : newjsp1
    Created on : 04/09/2021, 05:06:51 PM
    Author     : Francisca Gómez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.Pais"%>
<%
    Pais p;
    p = (Pais) request.getAttribute("Pais");

%>
<!DOCTYPE html>


<html lang="es">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            OMINISIS
        </title>
        <link rel="icon" href="img/favicon.ico">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="demo/demo.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/estilos.css">
    </head>

    <body class="user-profile">
        <div class="wrapper ">
            <div class="sidebar" data-color="red">
                <!--
                  Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
                -->
                <div class="logo">
                    <a href="#" class="simple-text logo-mini">

                    </a>
                    <a href="Principal.jsp" class="simple-text logo-normal">
                        OMNISIS
                    </a>
                </div>
                <div class="sidebar-wrapper" id="sidebar-wrapper">
                    <ul class="nav">
                        <li class="dropdown"> 
                            <a href="#">
                                <form action="pais" method="POST">                                   
                                    <input id="card-holder" type="text" name="buscartxt" placeholder="Ej: Paraguay" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[[A-Z]{1}[a-z]{2}\D{1,}" title="Paraguay" id="descri" >
                                    <span>
                                        <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Buscar">
                                    </span>                            
                                </form>
                            </a>
                        </li>    
                        <li>
                            <a href="#">
                                <form action="pais" method="POST">
                                    <input type="hidden" name="buscartxt" value=" ">                             
                                    <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Activo">                              
                                </form>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <form action="pais" method="POST">
                                    <input type="hidden" name="buscartxt" value=" ">                             
                                    <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Inactivo">                              
                                </form>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <form action="pais" method="POST">
                                    <input type="hidden" name="buscartxt" value=" ">                             
                                    <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Todos">                              
                                </form>
                            </a>
                        </li>                       
                    </ul>
                </div>
            </div>
            <div class="main-panel" id="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <div class="navbar-toggle">
                                <button type="button" class="navbar-toggler">
                                    <span class="navbar-toggler-bar bar1"></span>
                                    <span class="navbar-toggler-bar bar2"></span>
                                    <span class="navbar-toggler-bar bar3"></span>
                                </button>
                            </div>
                            <a class="navbar-brand" href="#">Pais</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                        </button>



                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="panel-header panel-header-sm">
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="title">Pais</h5>
                                </div>
                                <div class="card-body">
                                    <form action="pais" method="POST">
                                        <div class="row">                                            
                                            <div class="row">
                                                <div class="col-md-6 pr-1">
                                                    <div class="form-group">
                                                        <label>Descripcion</label>
                                                        <% if (p.getDescripcion_pais() != null) {%>  <%--para sacar null--%>
                                                        <input id="card-holder" type="text" name="descriptxt" class="form-control" onreset="true" placeholder="Ej: Paraguay" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[[A-Z]{1}[a-z]{2}\D{1,}" title="Paraguay" id="descri" value="<%= p.getDescripcion_pais()%>">
                                                        <%  } else {%>
                                                        <input id="card-holder" type="text" name="descriptxt" class="form-control" placeholder="Ej: Paraguay" aria-label="Card Holder" aria-describedby="basic-addon1" pattern="[[A-Z]{1}[a-z]{2}\D{1,}" title="Paraguay" id="descri" value="">
                                                        <% }%> 
                                                        <input id="card-holder" type="hidden" name="idtxt" class="form-control" aria-label="Card Holder" aria-describedby="basic-addon1" value="<%= p.getId_pais()%>">
                                                    </div>
                                                </div>

                                                <div class="form-group col-sm-7">
                                                    <label for="card-holder">Estado</label>
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 d-flex align-items-center justify-content-between">
                                                            <div class="custom-control custom-radio">
                                                                <% if (p.getEstado_pais() == "activo") { %>
                                                                <input class="custom-control-input" id="customRadio1" type="radio" checked name="customRadio" value="activo">
                                                                <% } else {%>
                                                                <input class="custom-control-input" id="customRadio1" type="radio" checked name="customRadio" value="activo">
                                                                <% }%>
                                                                <label class="custom-control-label" for="customRadio1">
                                                                    <p class="mb-0">Activo</p>
                                                                </label>
                                                            </div>
                                                            <label for="customRadio1"><img src="https://res.cloudinary.com/mhmd/image/upload/v1579682182/2_rqo4zs.gif" alt="" width="60"></label>
                                                        </li>
                                                        <li class="list-group-item d-flex align-items-center justify-content-between">
                                                            <div class="custom-control custom-radio">
                                                                <% if (p.getEstado_pais() == "inactivo") {%>
                                                                <input class="custom-control-input" id="customRadio2" type="radio" checked="true" name="customRadio" value="inactivo">
                                                                <% } else {%>
                                                                <input class="custom-control-input" id="customRadio2" type="radio" name="customRadio" value="inactivo">
                                                                <% }%>
                                                                <label class="custom-control-label" for="customRadio2">
                                                                    <p class="mb-0">Inactivo</p>
                                                                </label>
                                                            </div>
                                                            <label for="customRadio2"><img src="https://res.cloudinary.com/mhmd/image/upload/v1579682182/1_ezgo0i.png" alt="" width="60"></label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>                                        
                                        </div> 
                                        <div>
                                            <nav class="navbar navbar-dark navbar-expand-md navigation-clean-search">
                                                <ul class="nav navbar-nav">
                                                    <li class="dropdown">
                                                        <input type="submit" name="accion" class="btn btn-danger btn-icon-split" value="Guardar">
                                                    </li>
                                                    <li class="dropdown">
                                                        <input type="submit" name="accion"  class="btn btn-danger btn-icon-split" value="Modificar" data-toggle="modal" data-target="#logoutModal">
                                                    </li>
                                                    <li class="dropdown">
                                                        <input type="reset" class="btn btn-danger btn-icon-split" value="Limpiar">
                                                    </li>
                                                    <a class="btn btn-danger btn-icon-split" role="button" href="referenciales.jsp">Atrás</a>
                                                </ul>
                                            </nav>
                                        </div> 
                                        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Modificación</h5>
                                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">Datos modificados correctamente.</div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-secondary" type="button" data-dismiss="modal">OK</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>                  
                </div>
            </div>
            <!--   Core JS Files   -->
            <script src="js/core/jquery.min.js"></script>
            <script src="js/core/popper.min.js"></script>
            <script src="js/core/bootstrap.min.js"></script>
            <script src="js/plugins/perfect-scrollbar.jquery.min.js"></script>
            <!--  Google Maps Plugin    -->
            <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
            <!-- Chart JS -->
            <script src="js/plugins/chartjs.min.js"></script>
            <!--  Notifications Plugin    -->
            <script src="js/plugins/bootstrap-notify.js"></script>
            <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
            <script src="js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
            <script src="demo/demo.js"></script>
    </body>

</html>
