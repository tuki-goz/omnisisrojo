<%-- 
    Document   : compraTodos
    Created on : 09/09/2021, 06:27:26 PM
    Author     : Francisca Gómez
--%>

<%@page import="model.Compra"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Compra com;
    com = (Compra) request.getAttribute("Compra");
%>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            OMNISIS
        </title>
        <link rel="icon" href="img/favicon.ico">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="demo/demo.css" rel="stylesheet" />

        <link rel="stylesheet" href="css/estilos.css">
    </head>

    <body class="">
        <%@ page import="model.Compra, java.util.ArrayList"  %>
        <%@ page import="controller.compra"  %>
        <%
            ArrayList<Compra> AA = null;
            AA = (ArrayList<Compra>) request.getAttribute("CompraArray");
        %>  
        <div class="wrapper ">
            <div class="sidebar" data-color="red">
                <!--
                  Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
                -->
                <div class="logo">
                    <a href="#" class="simple-text logo-mini">

                    </a>
                    <a href="#" class="simple-text logo-normal">
                        OMNISIS
                    </a>
                </div>
                <div class="sidebar-wrapper" id="sidebar-wrapper">
                    <ul class="nav"> 
                        <li class="dropdown"> 
                            <a href="#">
                                <form action="compra" method="POST">  
                                    <a href="#" class="simple-text logo-normal">
                                        <input type="submit" name="accion" class="btn btn-light action-button" value="Volver">                              
                                    </a>
                                </form>   
                            </a>
                        </li> 
                    </ul>
                </div>
            </div>
            <div class="main-panel" id="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <div class="navbar-toggle">
                                <button type="button" class="navbar-toggler">
                                    <span class="navbar-toggler-bar bar1"></span>
                                    <span class="navbar-toggler-bar bar2"></span>
                                    <span class="navbar-toggler-bar bar3"></span>
                                </button>
                            </div>
                            <a class="navbar-brand" href="#pablo">Todas las Compras</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                        </button>                       
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="panel-header panel-header-sm">
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title"> Compras</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead class=" text-primary">
                                            <th>
                                                Opción
                                            </th>
                                            <th>
                                                Código
                                            </th>
                                            <th>
                                                Estado
                                            </th>
                                            <th>
                                                Nro Contrato
                                            </th>
                                            </thead>
                                            <%
                                                if (AA != null) {
                                            %> 
                                            <%
                                                for (Compra compra : AA) {
                                            %>                                            
                                            <tr>
                                                <td class="text-center"> 
                                                    <form action="compra" method="POST">
                                                        <input id="card-holder" type="hidden" name="buscartxt" value="<%= compra.getId_compra()%>">
                                                        <span>
                                                                <input type="submit" name="accion" target="_blank" class="btn btn-danger btn-icon-split" value="Ver">
                                                        </span>
                                                    </form>
                                                </td>                                               
                                                <td class="text-center"> 
                                                    <%= compra.getCodigo()%>
                                                </td>                                               
                                                <td class="text-center"> 
                                                    <%= compra.getNro_contrato()%>
                                                </td>
                                                <td class="text-center"> 
                                                    <%= compra.getDescrip_estado()%>
                                                </td>

                                            </tr>
                                            <% } %>
                                            <% } %>
                                            <% AA.size();%>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="js/core/jquery.min.js"></script>
        <script src="js/core/popper.min.js"></script>
        <script src="js/core/bootstrap.min.js"></script>
        <script src="js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!--  Google Maps Plugin    -->
        <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
        <!-- Chart JS -->
        <script src="js/plugins/chartjs.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src=".js/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
        <script src="demo/demo.js"></script>
    </body>

</html>

