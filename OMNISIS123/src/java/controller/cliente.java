/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Cliente;
import model.Variables;

/**
 *
 * @author Francisca Gómez
 */
@WebServlet(name = "cliente", urlPatterns = {"/cliente"})
public class cliente extends HttpServlet {

    Cliente clie = new Cliente();
    Cliente cliebd = new Cliente();
    int r;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         String accion = request.getParameter("accion");
         
        if (accion.equals("Guardar")) {            
            String desc = request.getParameter("descriptxt");
            String nroDoc = request.getParameter("nroDoctxt");
            String tel = request.getParameter("teltxt");
            String email = request.getParameter("emailtxt");
            String dir = request.getParameter("dirtxt");
            String estado = request.getParameter("customRadio");
            String usu = Variables.usuario1;
            int barrio = Integer.valueOf(request.getParameter("drop_bar"));
            int length = desc.length();
            int length2 = nroDoc.length();
            int length3 = email.length();
            int length4 = tel.length();
            int length5 = dir.length();
            if (length == 0||length2 == 0||length3 == 0||length4 == 0||length5 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                clie.setDesc_clie(desc);
                clie.setNro_doc_clie(nroDoc);
                clie.setTel_clie(tel);
                clie.setEmail_clie(email);
                clie.setDir_clie(dir);
                clie.setEstado_clie(estado);
                clie.setNom_usu_mod(usu);
                clie.setId_bar(barrio);
                r = cliebd.registrarCliente(clie);
                if (r == 1) {
                    request.getRequestDispatcher("registradoReferenciales.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
         if (accion.equals("Buscar")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            clie = clie.buscarCliente(buscar);
            request.setAttribute("Cliente", clie);
            request.getRequestDispatcher("cliente.jsp").forward(request, response);                                 
        }
        if (accion.equals("Cliente")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            clie = clie.buscarCliente(buscar);
            request.setAttribute("Cliente", clie);
            request.getRequestDispatcher("cliente.jsp").forward(request, response);
        }
        
        if (accion.equals("Modificar")) {                       
            String desc = request.getParameter("descriptxt");
            String nroDoc = request.getParameter("nroDoctxt");
            String tel = request.getParameter("teltxt");
            String email = request.getParameter("emailtxt");
            String dir = request.getParameter("dirtxt");
            String estado = request.getParameter("customRadio");
            String usu = Variables.usuario1;
            int barrio = Integer.valueOf(request.getParameter("drop_bar"));
            int id_clie = Integer.valueOf(request.getParameter("idtxt"));
            int length = desc.length();
            int length2 = nroDoc.length();
            int length3 = email.length();
            int length4 = tel.length();
            int length5 = dir.length();
            if (length == 0||length2 == 0||length3 == 0||length4 == 0||length5 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                clie.setDesc_clie(desc);
                clie.setNro_doc_clie(nroDoc);
                clie.setTel_clie(tel);
                clie.setEmail_clie(email);
                clie.setDir_clie(dir);
                clie.setEstado_clie(estado);
                clie.setNom_usu_mod(usu);
                clie.setId_bar(barrio);
                clie.setId_clie(id_clie);
                r = cliebd.modificarCliente(clie);
                if (r == 0) {
                    request.getRequestDispatcher("registradoReferenciales.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
