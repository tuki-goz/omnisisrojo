/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ConectaBD;
import model.Usuario01;
import model.Variables;

/**
 *
 * @author Francisca Gómez
 */
@WebServlet(name = "usuario", urlPatterns = {"/usuario"})
public class usuario extends HttpServlet {

    Usuario01 usu = new Usuario01();
    Usuario01 usubd = new Usuario01();
    int r;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         String accion = request.getParameter("accion");
        if (accion.equals("Guardar")) {
            String nom = request.getParameter("nombretxt");
            String email = request.getParameter("emailtxt");
            String clave = request.getParameter("clavetxt");
            String estado = request.getParameter("customRadio");
            String usuar = Variables.usuario1;
            int per = Integer.valueOf(request.getParameter("drop_per"));
            int fun = Integer.valueOf(request.getParameter("drop_fun"));
            int length = nom.length();
            int length2 = email.length();
            int length3 = clave.length();
            if (length == 0 || length2 == 0 || length3 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                usu.setNom_usu(nom);
                usu.setEmail_usu(email);
                usu.setClave_usu(clave);
                usu.setEstado_usu(estado);
                usu.setNom_usu_mod(usuar);
                usu.setId_per(per);
                usu.setId_fun(fun);
                r = usubd.registrarUsuario(usu);
                if (r == 0) {
                    request.getRequestDispatcher("usuarioguardado.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
        
        if (accion.equals("Buscar")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            usu = usu.buscarUsuario(buscar);
            request.setAttribute("Usuarios", usu);
            request.getRequestDispatcher("usuario.jsp").forward(request, response);                                
        }
        
        if (accion.equals("Usuarios")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            usu = usu.buscarUsuario(buscar);
            request.setAttribute("Usuarios", usu);
            request.getRequestDispatcher("usuario.jsp").forward(request, response);                                
        }
        
        if (accion.equals("Modificar")) {            
            String nom = request.getParameter("nombretxt");
            String email = request.getParameter("emailtxt");
            String clave = request.getParameter("clavetxt");
            String estado = request.getParameter("customRadio");
            String usuar = Variables.usuario1;
            int per = Integer.valueOf(request.getParameter("drop_per"));
            int fun = Integer.valueOf(request.getParameter("drop_fun"));
            int usuario = Integer.valueOf(request.getParameter("idtxt"));
            int length = nom.length();
            int length2 = email.length();
            int length3 = clave.length();
            if (length == 0 || length2 == 0 || length3 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                usu.setNom_usu(nom);
                usu.setEmail_usu(email);
                usu.setClave_usu(clave);
                usu.setEstado_usu(estado);
                usu.setNom_usu_mod(usuar);
                usu.setId_per(per);
                usu.setId_fun(fun);
                usu.setId_usua(usuario);
                r = usubd.modificarUsuario(usu);
                if (r == 0) {
                    request.getRequestDispatcher("usuariomodificado.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
        
        if (accion.equals("Restablecer")) {            
            String nom = request.getParameter("nombretxt");
            int length = nom.length();
            if (length == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                usu.setNom_usu(nom);
                r = usubd.restablecerUsuario(usu);
                if (r == 1) {
                    request.getRequestDispatcher("usuariorestablecido.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
