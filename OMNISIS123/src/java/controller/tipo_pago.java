/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Tipo_pago;

/**
 *
 * @author Francisca Gómez
 */
@WebServlet(name = "tipo_pago", urlPatterns = {"/tipo_pago"})
public class tipo_pago extends HttpServlet {

    Tipo_pago tp = new Tipo_pago();
    Tipo_pago tpbd = new Tipo_pago();
    int r;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String accion = request.getParameter("accion");
        if (accion.equals("Guardar")) {
            String descrip = request.getParameter("descriptxt");
            int length = descrip.length();
            if (length == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                tp.setDescrip_tp(descrip);
                r = tpbd.registrarTipoPago(tp);
                if (r == 0) {
                    request.getRequestDispatcher("registradoReferenciales.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
        
        if (accion.equals("Buscar")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();

            tp = tp.buscarTipoPago(buscar);
            request.setAttribute("Tipo de Pago", tp);
            request.getRequestDispatcher("tipo_de_pago.jsp").forward(request, response);
                                  

        }
        if (accion.equals("Tipo de Pago")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();

            tp = tp.buscarTipoPago(buscar);
            request.setAttribute("Tipo de Pago", tp);
            request.getRequestDispatcher("tipo_de_pago.jsp").forward(request, response);
                                  

        }
        
        if (accion.equals("Modificar")) {            
            String descrip = request.getParameter("descriptxt");
            int id_tp = Integer.valueOf(request.getParameter("idtxt"));
            int length = descrip.length();
            if (length == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                tp.setDescrip_tp(descrip);
                tp.setId_tp(id_tp);
                r = tpbd.modificarTipoPago(tp);
                if (r == 0) {
                    request.getRequestDispatcher("registradoReferenciales.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
