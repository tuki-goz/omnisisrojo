/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Variables;
import model.Aprobacioncompra;

/**
 *
 * @author Alvaro
 */
@WebServlet(name = "aprobaciondecompra", urlPatterns = {"/aprobaciondecompra"})
public class aprobacioncompra extends HttpServlet {

    Aprobacioncompra apr = new Aprobacioncompra();
    Aprobacioncompra aprbd = new Aprobacioncompra();
    int r;
    boolean x;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String accion = request.getParameter("accion");

        if (accion.equals("Guardar")) {

            String descri_apro = request.getParameter("descriptxt");
            String nro_apro = request.getParameter("aprobtxt");
            String estado_apro = request.getParameter("customRadio");
            String usu = Variables.usuario1;
            int id_usuario = Integer.valueOf(request.getParameter("drop_usu"));
            int id_pedido = Integer.valueOf(request.getParameter("dropped_ped"));
            int length = descri_apro.length();
            int length2 = nro_apro.length();
            //int length3 = estado_apro.length();

            if (length == 0 || length2 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {

                apr.setDescripcion_aprobacion_compra(descri_apro);
                apr.setNro_aprobacion(nro_apro);
                apr.setEstado_aprobacion(estado_apro);
                apr.setNom_usu(usu);
                apr.setId_usuario(id_usuario);
                apr.setId_pedido(id_pedido);

                r = aprbd.registrarAprobacioncompra(apr);
                if (r == 1) {  //modificado, porque entraba con 1 al error general jsp
                    request.getRequestDispatcher("aprobaciondecompraguardado.jsp").forward(request, response);

                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
        if (accion.equals("Buscar")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            apr = apr.buscarAprobacioncompra(buscar);
            request.setAttribute("Aprobacion", apr);
            request.getRequestDispatcher("aprobaciondecompra.jsp").forward(request, response);

        }
        if (accion.equals("Aprobacion")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            apr = apr.buscarAprobacioncompra(buscar);
            request.setAttribute("Aprobacion", apr);
            request.getRequestDispatcher("aprobaciondecompra.jsp").forward(request, response);

        }

        if (accion.equals("Modificar")) {
            String descrip = request.getParameter("descriptxt");
            String descri_apro = request.getParameter("descriptxt");
            String nro_apro = request.getParameter("aprobtxt");
            String estado_apro = request.getParameter("customRadio");
            String usu = Variables.usuario1;
            int id_usuario = Integer.valueOf(request.getParameter("drop_usu"));
            int id_pedido = Integer.valueOf(request.getParameter("dropped_ped"));
            int length = descri_apro.length();
            int length2 = nro_apro.length();
            if (length == 0 || length2 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {

                apr.setDescripcion_aprobacion_compra(descri_apro);
                apr.setNro_aprobacion(nro_apro);
                apr.setEstado_aprobacion(estado_apro);
                apr.setNom_usu(usu);
                apr.setId_usuario(id_usuario);
                apr.setId_pedido(id_pedido);

                r = aprbd.modificarAprobacion(apr);
                if (r == 1) {  //modificado, porque entraba con 1 al error general jsp
                    request.getRequestDispatcher("registradoCompras.jsp").forward(request, response);

                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
