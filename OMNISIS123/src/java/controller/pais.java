/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Pais;

/**
 *
 * @author Francisca Gómez
 */
@WebServlet(name = "pais", urlPatterns = {"/pais"})
public class pais extends HttpServlet {

    Pais p = new Pais();
    Pais pbd = new Pais();
    int r;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String accion = request.getParameter("accion");
        ArrayList<Pais> pais = new ArrayList();
        Pais leer = new Pais();
        pais = leer.leerPaisAc();
        pais = leer.leerPaisIna();
        pais = leer.leerPaisTo();
        if (accion.equals("Guardar")) {
            String descrip = request.getParameter("descriptxt");
            String estado = request.getParameter("customRadio");
            int length = descrip.length();
            if (length == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                p.setDescripcion_pais(descrip);
                p.setEstado_pais(estado);
                r = pbd.registrarPais(p);
                if (r == 0) {
                    request.getRequestDispatcher("paisguardado.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }

        if (accion.equals("Buscar")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            p = p.buscarPais(buscar);
            request.setAttribute("Pais", p);
            request.getRequestDispatcher("newjsp1.jsp").forward(request, response);
        }

        if (accion.equals("Pais")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            p = p.buscarPais(buscar);
            request.setAttribute("Pais", p);
            request.getRequestDispatcher("newjsp1.jsp").forward(request, response);
        }

        if (accion.equals("Modificar")) {
            String descrip = request.getParameter("descriptxt");
            String estado = request.getParameter("customRadio");
            int id_p = Integer.valueOf(request.getParameter("idtxt"));
            int length = descrip.length();
            if (length == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                p.setDescripcion_pais(descrip);
                p.setEstado_pais(estado);
                p.setId_pais(id_p);
                r = pbd.modificarPais(p);
                if (r == 0) {
                    request.setAttribute("Pais", p);
                    request.getRequestDispatcher("newjsp1.jsp").forward(request, response);                    
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
        if (accion.equals("Activo")) {
            pais = leer.leerPaisAc();
            request.setAttribute("PaisL",pais);
            request.setAttribute("Pais", p);
            request.getRequestDispatcher("newjsp2.jsp").forward(request, response);
        }
        if (accion.equals("Inactivo")) {
            pais = leer.leerPaisIna();
            request.setAttribute("PaisL",pais);
            request.setAttribute("Pais", p);
            request.getRequestDispatcher("newjsp3.jsp").forward(request, response);
        }
        if (accion.equals("Todos")) {
            pais = leer.leerPaisTo();
            request.setAttribute("PaisL",pais);
            request.setAttribute("Pais", p);
            request.getRequestDispatcher("newjsp4.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
