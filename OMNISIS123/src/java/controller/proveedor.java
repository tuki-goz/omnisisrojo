/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Proveedor;
import model.Variables;

/**
 *
 * @author Francisca Gómez
 */
@WebServlet(name = "proveedor", urlPatterns = {"/proveedor"})
public class proveedor extends HttpServlet {

    
    Proveedor prov = new Proveedor();
    Proveedor prvbd = new Proveedor();
    int r;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String accion = request.getParameter("accion");
        if (accion.equals("Guardar")) {            
            String nom = request.getParameter("nombretxt");
            String proc = request.getParameter("proctxt");
            String ruc = request.getParameter("ructxt");
            String tel = request.getParameter("teltxt");
            String dir = request.getParameter("dirtxt");
            String estado = request.getParameter("customRadio");
            String usu = Variables.usuario1;
            int pais = Integer.valueOf(request.getParameter("drop_pais"));
            int length = nom.length();
            int length2 = proc.length();
            int length3 = ruc.length();
            int length4 = tel.length();
            int length5 = dir.length();
            if (length == 0||length2 == 0||length3 == 0||length4 == 0||length5 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                prov.setNom_prov(nom);
                prov.setProc_prov(proc);
                prov.setRuc_prov(ruc);
                prov.setTel_prov(tel);
                prov.setDir_prov(dir);
                prov.setEstado_prov(estado);
                prov.setNom_usu_mod(usu);
                prov.setId_pais(pais);
                r = prvbd.registrarProveedor(prov);
                if (r == 1) {
                    request.getRequestDispatcher("registradoReferenciales.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
         if (accion.equals("Buscar")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            prov = prov.buscarProveedor(buscar);
            request.setAttribute("Proveedor", prov);
            request.getRequestDispatcher("proveedor.jsp").forward(request, response);
        }
         
        if (accion.equals("Proveedor")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            prov = prov.buscarProveedor(buscar);
            request.setAttribute("Proveedor", prov);
            request.getRequestDispatcher("proveedor.jsp").forward(request, response);
        }
        
        if (accion.equals("Modificar")) {            
            String nom = request.getParameter("nombretxt");
            String proc = request.getParameter("proctxt");
            String ruc = request.getParameter("ructxt");
            String tel = request.getParameter("teltxt");
            String dir = request.getParameter("dirtxt");
            String estado = request.getParameter("customRadio");
            String usu = Variables.usuario1;
            int pais = Integer.valueOf(request.getParameter("drop_pais"));
            int id_prov = Integer.valueOf(request.getParameter("idtxt"));
            int length = nom.length();
            int length2 = proc.length();
            int length3 = ruc.length();
            int length4 = tel.length();
            int length5 = dir.length();
            if (length == 0||length2 == 0||length3 == 0||length4 == 0||length5 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                prov.setNom_prov(nom);
                prov.setProc_prov(proc);
                prov.setRuc_prov(ruc);
                prov.setTel_prov(tel);
                prov.setDir_prov(dir);
                prov.setEstado_prov(estado);
                prov.setNom_usu_mod(usu);
                prov.setId_pais(pais);
                prov.setId_prov(id_prov);
                r = prvbd.modificarProveedor(prov);
                if (r == 0) {
                    request.getRequestDispatcher("registradoReferenciales.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
