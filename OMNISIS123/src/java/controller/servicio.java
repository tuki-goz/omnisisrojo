/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Variables;
import model.Servicio;

/**
 *
 * @author Alvaro
 */
@WebServlet(name = "servicio", urlPatterns = {"/servicio"})
public class servicio extends HttpServlet {

    Servicio ser = new Servicio();
    Servicio serbd = new Servicio();
    int r;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String accion = request.getParameter("accion");

        if (accion.equals("Guardar")) {

            String numero_ser = request.getParameter("numerotxt");
            String descrip_ser = request.getParameter("descriptxt");
            int tipo_ser = Integer.valueOf(request.getParameter("dropts_ts"));
            Double precio_ser = Double.parseDouble(request.getParameter("preciotxt"));
            String iva_ser = request.getParameter("ivatxt");
            String fecha_ser = request.getParameter("fechasertxt");
            String estado_ser = request.getParameter("customRadio");
            int id_usu_ser = Integer.valueOf(request.getParameter("dropusu_usu"));
            String usu = Variables.usuario1;
            int id_contrato = Integer.valueOf(request.getParameter("drop_cont"));
            int id_impuesto = Integer.valueOf(request.getParameter("drop_imp"));
            String usu_carga = Variables.usuario1;
            String notas = request.getParameter("notastxt");
            String clie_cont = request.getParameter("drop_clie");

            int length = numero_ser.length();
            int length2 = descrip_ser.length();
            int length3 = iva_ser.length();
            int length4 = fecha_ser.length();
            int length5 = estado_ser.length();
            if (length == 0 || length2 == 0 || length3 == 0 || length4 == 0 || length5 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {

                ser.setNumero_servicio_tecnico(numero_ser);
                ser.setDescripcion_servicio_tecnico(descrip_ser);
                ser.setId_tipo_servicio_tecnico(tipo_ser);
                ser.setPrecio_servicio_tecnico(precio_ser);
                ser.setIva_servicio_tecnico(iva_ser);
                ser.setFecha_servicio_tecnnico(fecha_ser);
                ser.setEstado_servicio_tecnico(estado_ser);
                ser.setId_usuario(id_usu_ser);
                ser.setNombre_usuario_modificacion(usu);
                ser.setId_contrato(id_contrato);
                ser.setId_impuesto(id_impuesto);
                ser.setUsuario_carga(usu_carga);
                ser.setNotas(notas);
                ser.setCliente_contrato(clie_cont);

                r = serbd.registrarServicio(ser);
                if (r == 0) {
                    request.getRequestDispatcher("registrarservicioguardado.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
        if (accion.equals("Buscar")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            ser = ser.buscarServicio(buscar);
            request.setAttribute("Servicio", ser);
            request.getRequestDispatcher("registrarservicio.jsp").forward(request, response);

        }
        if (accion.equals("Servicio")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            ser = ser.buscarServicio(buscar);
            request.setAttribute("Servicio", ser);
            request.getRequestDispatcher("registrarservicio.jsp").forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
