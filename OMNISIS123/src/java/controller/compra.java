/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Compra;
import model.Variables;

/**
 *
 * @author Francisca Gómez
 */
@WebServlet(name = "compra", urlPatterns = {"/compra"})
public class compra extends HttpServlet {

    Compra com = new Compra();
    Compra com1 = new Compra();
    Compra com3 = new Compra();
    Compra combd = new Compra();
    Compra combd1 = new Compra();
    int r;
    boolean x;
    Double aa;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String accion = request.getParameter("accion");
        PrintWriter out = response.getWriter();
        ArrayList<Compra> compra = new ArrayList<Compra>();
        ArrayList<Compra> compraDetalle = new ArrayList<Compra>();
        ArrayList<Compra> compraTotal = new ArrayList<Compra>();
        ArrayList<Compra> compraLiquidacion = new ArrayList<Compra>();
        Compra leer = new Compra();
        compraDetalle = leer.leerCompra();
        if (compraDetalle != null) {
            request.setAttribute("CompraDet", compraDetalle);
            request.setAttribute("CompraSub", compraTotal);
            request.setAttribute("CompraLiq", compraLiquidacion);
            if (accion.equals("Ingresar")) {
                Variables.ban = 0;
                com = new Compra();  //instanciamos esta clase para almacenar los datos de los txt en el jsp, para que no se pierdan cuando se carga el array

                /////contenido de la cabecera
                com.setCodigo(request.getParameter("codtxt")); // se cambió la forma de acceder a los datos, se eliminaron las variables locales.
                com.setFecha_compra(request.getParameter("fechacompra"));
                com.setId_proveedor(Integer.valueOf(request.getParameter("drop_prov")));
                com.setId_usuario(Integer.valueOf(request.getParameter("drop_usu")));
                com.setId_moneda(Integer.valueOf(request.getParameter("dropmon_mon")));
                com.setId_tipo_p(Integer.valueOf(request.getParameter("drop_tipo")));
                com.setId_forma_c(Integer.valueOf(request.getParameter("drop_form")));
                com.setId_estado(Integer.valueOf(request.getParameter("drop_est")));
                com.setNro_contrato(request.getParameter("nroctxt"));
                int Idcaptura = 0;

                ////contenido del array//////
                String descripcion = request.getParameter("descriptxt");
                int cantidad = Integer.valueOf(request.getParameter("cantidadtxt"));
                //Double iva5 = Double.valueOf(request.getParameter("iva5txt"));
                //Double iva10 = Double.valueOf(request.getParameter("iva10txt"));
                //Double exenta = Double.valueOf(request.getParameter("exentxt"));
                Double precio = Double.valueOf(request.getParameter("prectxt"));
                int orden = Integer.valueOf(request.getParameter("dropord_ord"));
                int impuesto = Integer.valueOf(request.getParameter("selectbasic"));

                try {
                    if (Idcaptura == 0) {
                        DecimalFormat df = new DecimalFormat("###.##");//COMENTARIO
                        Variables.tota_compra = Variables.tota_compra + (precio * cantidad);
                        if (impuesto == 21) {
                            Variables.iva5 = Math.floor(precio / impuesto);
                            df.format(Variables.iva5);
                            Variables.totalliq5 = Variables.totalliq5 + (Variables.iva5);
                        }
                        if (impuesto == 11) {
                            Variables.iva10 = Math.floor(precio / impuesto);
                            Variables.totalliq10 = Variables.totalliq10 + (Variables.iva10);
                        }
                        if (impuesto == 0) {
                            Variables.exenta = Math.floor(precio * impuesto);
                        }
                        Variables.sub_total = Variables.sub_total + (precio);
                        Variables.totalliqui = Variables.totalliqui + (Variables.iva5 + Variables.iva10);

                        Compra com01 = new Compra();
                        Compra com02 = new Compra();
                        Compra com03 = new Compra();
                        com01.setCantidad_Det(cantidad);
                        com01.setDescripcion_Det(descripcion);
                        com01.setIva5_Det(Variables.iva5);
                        com01.setIva10_Det(Variables.iva10);
                        com01.setExenta_Det(Variables.exenta);
                        com01.setPrecio_Det(precio);
                        com01.setId_orden_compra(orden);
                        com02.setSub_total(Variables.sub_total);
                        com02.setTotal_compra(Variables.tota_compra);
                        com03.setLiq5(Variables.totalliq5);
                        com03.setLiq10(Variables.totalliq10);
                        com03.setTotaliva(Variables.totalliqui);
                        Variables.addCompras(com01);
                        Variables.addCompras1(com02);
                        Variables.addCompras2(com03);

                    } else {

                    }
                    request.setAttribute("CompraDet", Variables.getCompras());
                    request.setAttribute("CompraSub", Variables.getCompras1());
                    request.setAttribute("CompraLiq", Variables.getCompras2());
                    request.setAttribute("Compra", com);
                    request.getRequestDispatcher("newjsp6.jsp").forward(request, response);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    out.close();
                }
            }
        }
        if (accion.equals("Guardar")) { ///agregado para hacer el comit solamente
            Variables.ban = 0;
            Variables.ban1 = 0;
            Variables.ban2 = 0;
            Variables.ocultar = 0;
            String codigo = request.getParameter("codtxt");
            String fecha = request.getParameter("fechacompra");
            int prov = Integer.valueOf(request.getParameter("drop_prov"));
            int mon = Integer.valueOf(request.getParameter("dropmon_mon"));
            int usu = Integer.valueOf(request.getParameter("drop_usu"));
            int tipo = Integer.valueOf(request.getParameter("drop_tipo"));
            String nro_cod = request.getParameter("nroctxt");
            int forma = Integer.valueOf(request.getParameter("drop_form"));
            int estado = Integer.valueOf(request.getParameter("drop_est"));
            String usua = Variables.usuario1;
            int length = codigo.length();
            int length3 = fecha.length();
            if (length == 0 || length3 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {

                com.setCod_contrato(codigo);
                com.setFecha_compra(fecha);
                com.setTotal_compra(Variables.tota_compra);
                com.setSub_total(Variables.sub_total);
                com.setNom_usu_mod(usua);
                com.setId_proveedor(prov);
                com.setId_moneda(mon);
                com.setId_usuario(usu);
                com.setId_tipo_p(tipo);
                com.setId_forma_c(forma);
                com.setId_estado(estado);
                com.setNro_contrato(nro_cod);
                com.setLiq5(Variables.totalliq5);
                com.setLiq10(Variables.totalliq10);
                com.setTotaliva(Variables.totalliqui);
                r = combd.registrarCompra(com);
                if (r == 1) {  //modificado, porque entraba con 1 al error general jsp
                    request.getRequestDispatcher("registrarcompraguardado.jsp").forward(request, response);

                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }

        if (accion.equals("Buscar")) {
            Variables.ban = 1;
            Variables.ban1 = 1;
            Variables.ban2 = 1;
            Variables.ocultar = 1;
            String buscar = request.getParameter("buscartxt");
            com = com.buscarCompra(buscar);
//            String cod = request.getParameter("idtxt");
            compraDetalle = leer.leerDetalle(buscar);
            compraTotal = leer.leerTotal(buscar);
            compraLiquidacion = leer.leerLiquidacion(buscar);

            if (Variables.ban3 == 1) {  //modificado, porque entraba con 1 al error general jsp
                request.setAttribute("Compra", com);
                request.setAttribute("CompraDet", compraDetalle);
                request.setAttribute("CompraSub", compraTotal);
                request.setAttribute("CompraLiq", compraLiquidacion);
                request.getRequestDispatcher("newjsp6.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
            }
        }

        if (accion.equals("Compra")) {
            String buscar = request.getParameter("buscartxt");
            com = com.buscarCompra(buscar);
            request.setAttribute("Compra", com);
            request.getRequestDispatcher("newjsp6.jsp").forward(request, response);

        }

        if (accion.equals("Cambiar")) { ///agregado para hacer el comit solamente           
            String id_compra_det = (request.getParameter("idCompraDet"));
            com = leer.leerUnDetalle(id_compra_det);
            request.setAttribute("Compra", com);
            request.setAttribute("CompraDet", compraDetalle);

            Variables.tota_compra = 0.0;
            Variables.sub_total = 0.0;
            Variables.totalliqui = 0.0;
            Variables.totalliq5 = 0.0;
            Variables.totalliq10 = 0.0;
            request.getRequestDispatcher("newjsp7.jsp").forward(request, response);
        }

        if (accion.equals("Registrar")) {
            Variables.ban = 0;
            Variables.ban1 = 0;
            Variables.ban2 = 0;
            Variables.ocultar = 0;
            int cantidad = Integer.valueOf(request.getParameter("cantidadtxt"));
            String descripcion = request.getParameter("descriptxt");
            Double precio = Double.valueOf(request.getParameter("prectxt"));
            int orden = Integer.valueOf(request.getParameter("dropord_ord"));
            int id = Integer.valueOf(request.getParameter("idtxt"));
            int id2 = Integer.valueOf(request.getParameter("idtxt2"));
            int impuesto = Integer.valueOf(request.getParameter("selectbasic"));

            int length = descripcion.length();
            if (length == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                Variables.tota_compra = Variables.tota_compra + (precio * cantidad);
                if (impuesto == 21) {
                    Variables.iva5 = Math.floor(precio / impuesto);
                    Variables.totalliq5 = Variables.totalliq5 + (Variables.iva5);
                }
                if (impuesto == 11) {
                    Variables.iva10 = Math.floor(precio / impuesto);
                    Variables.totalliq10 = Variables.totalliq10 + (Variables.iva10);
                }
                if (impuesto == 0) {
                    Variables.exenta = Math.floor(precio * impuesto);
                }
                Variables.sub_total = Variables.sub_total + (precio);
                Variables.totalliqui = Variables.totalliqui + (Variables.iva5 + Variables.iva10);

                com.setCantidad_Det(cantidad);
                com.setDescripcion_Det(descripcion);
                com.setIva5_Det(Variables.iva5);
                com.setIva10_Det(Variables.iva10);
                com.setExenta_Det(Variables.exenta);
                com.setPrecio_Det(precio);
                com.setId_orden_compra(orden);
                com.setId_compra_Det(id);
                //SUBTOTAL
                com1.setTotal_compra(Variables.tota_compra);
                com1.setSub_total(Variables.sub_total);
                com1.setId_compra(id2);
                //LIQUIDACION                
                com3.setLiq5(Variables.totalliq5);
                com3.setLiq10(Variables.totalliq10);
                com3.setTotaliva(Variables.totalliqui);
                com3.setId_compra(id2);

                r = combd.modificarDetalle(com);
                r = combd1.modificarSub(com1);
                r = combd.modificarLiquidacion(com3);
                if (r == 1) {

                    String buscar = request.getParameter("idtxt2");
                    com = com.buscarCompra(buscar);
                    compraDetalle = leer.leerDetalle(buscar);
                    compraTotal = leer.leerTotal(buscar);
                    compraLiquidacion = leer.leerLiquidacion(buscar);

                    request.setAttribute("Compra", com);
                    request.setAttribute("CompraDet", compraDetalle);
                    request.setAttribute("CompraSub", compraTotal);
                    request.setAttribute("CompraLiq", compraLiquidacion);
                    request.getRequestDispatcher("newjsp6.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }

        }

        if (accion.equals("Modificar")) {
            Variables.ban = 0;
            Variables.ban1 = 0;
            Variables.ban2 = 0;
            Variables.ocultar = 0;
            int id = Integer.valueOf(request.getParameter("idtxt"));
            String codigo = request.getParameter("codtxt");
            String fecha = request.getParameter("fechacompra");
            int prov = Integer.valueOf(request.getParameter("drop_prov"));
            int mon = Integer.valueOf(request.getParameter("dropmon_mon"));
            int usu = Integer.valueOf(request.getParameter("drop_usu"));
            int tipo = Integer.valueOf(request.getParameter("drop_tipo"));
            String nro_cod = request.getParameter("nroctxt");
            int forma = Integer.valueOf(request.getParameter("drop_form"));
            int estado = Integer.valueOf(request.getParameter("drop_est"));
            String usua = Variables.usuario1;
            com.setCod_contrato(codigo);
            com.setFecha_compra(fecha);
            com.setTotal_compra(Variables.tota_compra);
            com.setSub_total(Variables.sub_total);
            com.setNom_usu_mod(usua);
            com.setId_proveedor(prov);
            com.setId_moneda(mon);
            com.setId_usuario(usu);
            com.setId_tipo_p(tipo);
            com.setId_forma_c(forma);
            com.setId_estado(estado);
            com.setNro_contrato(nro_cod);
            com.setLiq5(Variables.totalliq5);
            com.setLiq10(Variables.totalliq10);
            com.setTotaliva(Variables.totalliqui);
            com.setId_compra(id);
            r = combd.modificarCompra(com);
            if (r == 1) {  //modificado, porque entraba con 1 al error general jsp
                request.getRequestDispatcher("registrarcompramodificado.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
            }
        }
        if (accion.equals("Atras")) {
            request.setAttribute("Compra", com);
            request.getRequestDispatcher("newjsp.jsp").forward(request, response);
        }
        if (accion.equals("Ver")) {
            Variables.ban = 1;
            Variables.ban1 = 1;
            Variables.ban2 = 1;
            Variables.ocultar = 1;
            String buscar = request.getParameter("buscartxt");
            com = com.buscarCompra(buscar);
            compraDetalle = leer.leerDetalle(buscar);
            compraTotal = leer.leerTotal(buscar);
            compraLiquidacion = leer.leerLiquidacion(buscar);
            if (Variables.ban3 == 1) {  //modificado, porque entraba con 1 al error general jsp
                request.setAttribute("Compra", com);
                request.setAttribute("CompraDet", compraDetalle);
                request.setAttribute("CompraSub", compraTotal);
                request.setAttribute("CompraLiq", compraLiquidacion);
                request.getRequestDispatcher("newjsp6.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
            }
        }
        if (accion.equals("OK")) {
            request.setAttribute("Compra", com);
            request.getRequestDispatcher("newjsp.jsp").forward(request, response);
        }
        if (accion.equals("Volver")) {
            request.setAttribute("Compra", com);
            request.setAttribute("CompraDet", compraDetalle);
            request.getRequestDispatcher("newjsp6.jsp").forward(request, response);
        }
        if (accion.equals("Todos")) {
            compra = leer.leerCompraTo();
            request.setAttribute("CompraArray", compra);
            request.setAttribute("Compra", com);
            request.getRequestDispatcher("compraTodos.jsp").forward(request, response);
        }
        if (accion.equals("Volver atrás")) {
            request.getRequestDispatcher("newjsp.jsp").forward(request, response);
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
