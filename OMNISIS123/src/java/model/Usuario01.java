/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Francisca Gómez
 */
public class Usuario01 implements ValidarUsuario {

//CONSTRUCTORES - SET - GET
    int id_usua, id_per, id_fun;
    String nom_usu, email_usu, clave_usu, estado_usu, nom_usu_mod, descr_per, descr_fun;

    public Usuario01() {
    }

    public Usuario01(int id_usua, int id_per, int id_fun, String nom_usu, String email_usu, String clave_usu, String estado_usu, String nom_usu_mod) {
        this.id_usua = id_usua;
        this.id_per = id_per;
        this.id_fun = id_fun;
        this.nom_usu = nom_usu;
        this.email_usu = email_usu;
        this.clave_usu = clave_usu;
        this.estado_usu = estado_usu;
        this.nom_usu_mod = nom_usu_mod;
    }

    public int getId_usua() {
        return id_usua;
    }

    public void setId_usua(int id_usua) {
        this.id_usua = id_usua;
    }

    public int getId_per() {
        return id_per;
    }

    public void setId_per(int id_per) {
        this.id_per = id_per;
    }

    public int getId_fun() {
        return id_fun;
    }

    public void setId_fun(int id_fun) {
        this.id_fun = id_fun;
    }

    public String getNom_usu() {
        return nom_usu;
    }

    public void setNom_usu(String nom_usu) {
        this.nom_usu = nom_usu;
    }

    public String getEmail_usu() {
        return email_usu;
    }

    public void setEmail_usu(String email_usu) {
        this.email_usu = email_usu;
    }

    public String getClave_usu() {
        return clave_usu;
    }

    public void setClave_usu(String clave_usu) {
        this.clave_usu = clave_usu;
    }

    public String getEstado_usu() {
        return estado_usu;
    }

    public void setEstado_usu(String estado_usu) {
        this.estado_usu = estado_usu;
    }

    public String getNom_usu_mod() {
        return nom_usu_mod;
    }

    public void setNom_usu_mod(String nom_usu_mod) {
        this.nom_usu_mod = nom_usu_mod;
    }

    public String getDescr_per() {
        return descr_per;
    }

    public void setDescr_per(String descr_per) {
        this.descr_per = descr_per;
    }

    public String getDescr_fun() {
        return descr_fun;
    }

    public void setDescr_fun(String descr_fun) {
        this.descr_fun = descr_fun;
    }

    //FUNCIONES REGISTRAR - BUSCAR - MODIFICAR - CAMBIAR ESTADO
    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;

    @Override
    public int registrarUsuario(Usuario01 usu) {
        int r = 0;
        String sql = "INSERT INTO public.usuario(\n"
                + "nombre_usuario, email_usuario, clave_usuario, estado_usuario, \n"
                + "nombre_usuario_modificacion, fecha_modificacion, id_perfil, id_funcionario)\n"
                + "VALUES (?, ?, ?, ?, ?, (SELECT current_date), ?, ?);";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, usu.getNom_usu());
            ps.setString(2, usu.getEmail_usu());
            ps.setString(3, usu.getClave_usu());
            ps.setString(4, usu.getEstado_usu());
            ps.setString(5, usu.getNom_usu_mod());
            ps.setInt(6, usu.getId_per());
            ps.setInt(7, usu.getId_fun());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                usu.setNom_usu(rs.getString("nombre_usuario"));
                usu.setEmail_usu(rs.getString("email_usuario"));
                usu.setClave_usu(rs.getString("clave_usuario"));
                usu.setEstado_usu(rs.getString("estado_usuario"));
                usu.setNom_usu_mod(rs.getString("nombre_usuario_modificacion"));
                usu.setId_per(rs.getInt("id_perfil"));
                usu.setId_fun(rs.getInt("id_funcionario"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }

        } catch (Exception e) {
            return 0;
        }
    }

    public HashMap seleccionarUsuario() {
        HashMap<String, String> drop_usu = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT id_usuario as id_usu,nombre_usuario\n"
                    + "FROM public.usuario\n"
                    + "WHERE estado_usuario = 'activo';";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                drop_usu.put(rs.getString("id_usu"), rs.getString("nombre_usuario"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return drop_usu;
    }

    public Usuario01 buscarUsuario(String buscartxt) {
        int r = 0;
        Usuario01 usu = new Usuario01();
        String sql = "SELECT usu.id_usuario, usu.nombre_usuario, usu.email_usuario, usu.clave_usuario,\n"
                + "usu.estado_usuario, per.descripcion_perfil, fun.nombre_funcionario\n"
                + "FROM usuario usu\n"
                + "INNER JOIN perfil per\n"
                + "ON usu.id_perfil = per.id_perfil\n"
                + "INNER JOIN funcionario fun\n"
                + "ON usu.id_funcionario = fun.id_funcionario\n"
                + "WHERE usu.nombre_usuario =?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, buscartxt);

            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                usu.setId_usua(rs.getInt("id_usuario"));
                usu.setNom_usu(rs.getString("nombre_usuario"));
                usu.setEmail_usu(rs.getString("email_usuario"));
                usu.setClave_usu(rs.getString("clave_usuario"));
                usu.setDescr_per(rs.getString("descripcion_perfil"));
                usu.setDescr_fun(rs.getString("nombre_funcionario"));
                usu.setEstado_usu(rs.getString("estado_usuario"));
            }

        } catch (Exception e) {
            System.out.println("hola");

        } finally {
            return usu;
        }
    }

    public int modificarUsuario(Usuario01 usu) {
        int r = 0;
        String sql = "UPDATE public.usuario\n"
                + "SET nombre_usuario=?, email_usuario=?, clave_usuario=?, estado_usuario=?, \n"
                + "nombre_usuario_modificacion=?, fecha_modificacion=(SELECT current_date), id_perfil=?, id_funcionario=?\n"
                + "WHERE id_usuario=?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, usu.getNom_usu());
            ps.setString(2, usu.getEmail_usu());
            ps.setString(3, usu.getClave_usu());
            ps.setString(4, usu.getEstado_usu());
            ps.setString(5, usu.getNom_usu_mod());
            ps.setInt(6, usu.getId_per());
            ps.setInt(7, usu.getId_fun());
            ps.setInt(8, usu.getId_fun());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                usu.setNom_usu(rs.getString("nombre_usuario"));
                usu.setEmail_usu(rs.getString("email_usuario"));
                usu.setClave_usu(rs.getString("clave_usuario"));
                usu.setDescr_per(rs.getString("descripcion_perfil"));
                usu.setDescr_fun(rs.getString("nombre_funcionario"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public int restablecerUsuario(Usuario01 usu) {
        String sql = "Update usuario \n"
                + "set intentos_fallidos = 0, clave_usuario = '123', estado_usuario = 'activo'\n"
                + "where nombre_usuario = ?";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, usu.getNom_usu());
            ps.executeUpdate();
            
            return 1;
            
        } catch (Exception e) {
            return 0;
        }
    }
}
