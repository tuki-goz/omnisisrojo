/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Francisca Gómez
 */
public class Estado {

    int id;
    String descrip;

    public Estado(int id, String descrip) {
        this.id = id;
        this.descrip = descrip;
    }

    public Estado() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;

    public HashMap seleccionarEstado() {
        HashMap<String, String> drop_est = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT id_estado as id_es,descrip_estado\n"
                    + "FROM public.estado;";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                drop_est.put(rs.getString("id_es"), rs.getString("descrip_estado"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return drop_est;
    }
}
