/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Francisca Gómez
 */
public class Tipo_pago implements ValidarTipoPago {

    //CONSTRUCTORES - SET - GET
    int id_tp;
    String descrip_tp;

    public Tipo_pago(int id_tp, String descrip_tp) {
        this.id_tp = id_tp;
        this.descrip_tp = descrip_tp;
    }

    public Tipo_pago() {
    }

    public int getId_tp() {
        return id_tp;
    }

    public void setId_tp(int id_tp) {
        this.id_tp = id_tp;
    }

    public String getDescrip_tp() {
        return descrip_tp;
    }

    public void setDescrip_tp(String descrip_tp) {
        this.descrip_tp = descrip_tp;
    }

    //FUNCIONES REGISTRAR - BUSCAR - MODIFICAR - CAMBIAR ESTADO
    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;

    @Override
    public int registrarTipoPago(Tipo_pago tp) {
        int r = 0;
        String sql = "INSERT INTO public.tipo_pago(\n"
                + "descripcion_tipo_pago)\n"
                + "VALUES (?);";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, tp.getDescrip_tp());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                Variables.id = Integer.parseInt(rs.getString("id_usuario"));
                tp.setDescrip_tp(rs.getString("descripcion_tipo_pago"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public HashMap seleccionarTipoPago() {
        HashMap<String, String> drop_tp = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT id_tipo_pago as id_tp,descripcion_tipo_pago\n"
                    + "FROM public.tipo_pago;";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                drop_tp.put(rs.getString("id_tp"), rs.getString("descripcion_tipo_pago"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return drop_tp;
    }

    public Tipo_pago buscarTipoPago(String buscartxt) {
        int r = 0;
        Tipo_pago tp = new Tipo_pago();
        String sql = "SELECT id_tipo_pago, descripcion_tipo_pago\n"
                + "FROM tipo_pago\n"
                + "WHERE descripcion_tipo_pago = ?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, buscartxt);
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                tp.setDescrip_tp(rs.getString("descripcion_tipo_pago"));
                tp.setId_tp(rs.getInt("id_tipo_pago"));
            }
        } catch (Exception e) {
            System.out.println("hola");
        } finally {
            return tp;
        }
    }

    public int modificarTipoPago(Tipo_pago tp) {
        int r = 0;
        String sql = "UPDATE public.tipo_pago\n"
                + "SET descripcion_tipo_pago=?\n"
                + "WHERE id_tipo_pago=?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, tp.getDescrip_tp());
            ps.setInt(2, tp.getId_tp());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                tp.setDescrip_tp(rs.getString("descripcion_tipo_pago"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }
}
