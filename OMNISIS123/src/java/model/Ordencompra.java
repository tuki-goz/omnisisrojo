/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import model.Aprobacioncompra;

/**
 *
 * @author Alvaro
 */
public class Ordencompra implements ValidarOrdenCompra {

    int id_orden_compra, id_orden_compra_detalle, id_producto, id_aprobacion, cantidad_orden_det, id_proveedor, nro_orden_detalle;
    String nro_orden, fecha_pedido_orden, fecha_pago_orden, direccion_entrega_orden, estado_orden, nom_usu, fecha_modificacion;
    String descrip_orden_det;
    Double precio_unit_orden_det, precio_total_orden_det, costo_total_orden_det;

    public Ordencompra() {
    }

    public Ordencompra(int id_orden_compra, int id_orden_compra_detalle, int id_producto, int id_aprobacion, int cantidad_orden_det, int id_proveedor, int nro_orden_detalle, String nro_orden, String fecha_pedido_orden, String fecha_pago_orden, String direccion_entrega_orden, String estado_orden, String nom_usu, String fecha_modificacion, String descrip_orden_det, Double precio_unit_orden_det, Double precio_total_orden_det, Double costo_total_orden_det) {
        this.id_orden_compra = id_orden_compra;
        this.id_orden_compra_detalle = id_orden_compra_detalle;
        this.id_producto = id_producto;
        this.id_aprobacion = id_aprobacion;
        this.cantidad_orden_det = cantidad_orden_det;
        this.id_proveedor = id_proveedor;
        this.nro_orden_detalle = nro_orden_detalle;
        this.nro_orden = nro_orden;
        this.fecha_pedido_orden = fecha_pedido_orden;
        this.fecha_pago_orden = fecha_pago_orden;
        this.direccion_entrega_orden = direccion_entrega_orden;
        this.estado_orden = estado_orden;
        this.nom_usu = nom_usu;
        this.fecha_modificacion = fecha_modificacion;
        this.descrip_orden_det = descrip_orden_det;
        this.precio_unit_orden_det = precio_unit_orden_det;
        this.precio_total_orden_det = precio_total_orden_det;
        this.costo_total_orden_det = costo_total_orden_det;
    }

    public int getId_orden_compra() {
        return id_orden_compra;
    }

    public void setId_orden_compra(int id_orden_compra) {
        this.id_orden_compra = id_orden_compra;
    }

    public int getId_orden_compra_detalle() {
        return id_orden_compra_detalle;
    }

    public void setId_orden_compra_detalle(int id_orden_compra_detalle) {
        this.id_orden_compra_detalle = id_orden_compra_detalle;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public int getId_aprobacion() {
        return id_aprobacion;
    }

    public void setId_aprobacion(int id_aprobacion) {
        this.id_aprobacion = id_aprobacion;
    }

    public int getCantidad_orden_det() {
        return cantidad_orden_det;
    }

    public void setCantidad_orden_det(int cantidad_orden_det) {
        this.cantidad_orden_det = cantidad_orden_det;
    }

    public int getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(int id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public int getNro_orden_detalle() {
        return nro_orden_detalle;
    }

    public void setNro_orden_detalle(int nro_orden_detalle) {
        this.nro_orden_detalle = nro_orden_detalle;
    }

    public String getNro_orden() {
        return nro_orden;
    }

    public void setNro_orden(String nro_orden) {
        this.nro_orden = nro_orden;
    }

    public String getFecha_pedido_orden() {
        return fecha_pedido_orden;
    }

    public void setFecha_pedido_orden(String fecha_pedido_orden) {
        this.fecha_pedido_orden = fecha_pedido_orden;
    }

    public String getFecha_pago_orden() {
        return fecha_pago_orden;
    }

    public void setFecha_pago_orden(String fecha_pago_orden) {
        this.fecha_pago_orden = fecha_pago_orden;
    }

    public String getDireccion_entrega_orden() {
        return direccion_entrega_orden;
    }

    public void setDireccion_entrega_orden(String direccion_entrega_orden) {
        this.direccion_entrega_orden = direccion_entrega_orden;
    }

    public String getEstado_orden() {
        return estado_orden;
    }

    public void setEstado_orden(String estado_orden) {
        this.estado_orden = estado_orden;
    }

    public String getNom_usu() {
        return nom_usu;
    }

    public void setNom_usu(String nom_usu) {
        this.nom_usu = nom_usu;
    }

    public String getFecha_modificacion() {
        return fecha_modificacion;
    }

    public void setFecha_modificacion(String fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

    public String getDescrip_orden_det() {
        return descrip_orden_det;
    }

    public void setDescrip_orden_det(String descrip_orden_det) {
        this.descrip_orden_det = descrip_orden_det;
    }

    public Double getPrecio_unit_orden_det() {
        return precio_unit_orden_det;
    }

    public void setPrecio_unit_orden_det(Double precio_unit_orden_det) {
        this.precio_unit_orden_det = precio_unit_orden_det;
    }

    public Double getPrecio_total_orden_det() {
        return precio_total_orden_det;
    }

    public void setPrecio_total_orden_det(Double precio_total_orden_det) {
        this.precio_total_orden_det = precio_total_orden_det;
    }

    public Double getCosto_total_orden_det() {
        return costo_total_orden_det;
    }

    public void setCosto_total_orden_det(Double costo_total_orden_det) {
        this.costo_total_orden_det = costo_total_orden_det;
    }

    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    PreparedStatement ps2;
    PreparedStatement ps3;
    PreparedStatement ps4;
    ResultSet rs;
    ResultSet rs2;
    private String sSQL = "";
    private String sSQL2 = "";

    public int registrarOrdencompra(Ordencompra ord) {
        int r = 0;
        int q1 = 0;

        try {

            con = cn.getConnection();
            //INSERTA EL REGISTRO A LA CABECERA
            String sql = "INSERT INTO public.orden_compra(\n"
                    + "nro_orden_compra, fecha_pedido_orden_compra, fecha_pago_orden_compra,\n"
                    + "direccion_entrega_orden_compra, estado_orden_compra, nombre_usuario_modificacion,\n"
                    + "fecha_modificacion, id_proveedor)\n"
                    + "	VALUES (?, ?, ?, ?, ?, ?, (SELECT current_date),?);";
            ps = con.prepareStatement(sql);

            ps.setString(1, ord.getNro_orden());
            ps.setString(2, ord.getFecha_pedido_orden());
            ps.setString(3, ord.getFecha_pago_orden());
            ps.setString(4, ord.getDireccion_entrega_orden());
            ps.setString(5, ord.getEstado_orden());
            ps.setString(6, ord.getNom_usu());
            ps.setInt(7, ord.getId_proveedor());

            ps.executeUpdate();
            //COMENZARA A INGRESAR EL DETALLE
            //AQUI COMIENZA EL WHILE
            String sql1 = "INSERT INTO public.orden_compra_detalle(\n"
                    + "nro_orden_compra_detalle, descripcion_orden_compra_detalle,\n"
                    + "cantidad_orden_compra_detalle, precio_unit_orden_compra_detalle,\n"
                    + "precio_total_orden_compra_detalle, costo_total_orden_compra_detalle, id_orden_compra,\n"
                    + "id_producto, id_aprobacion_compra)\n"
                    + "	VALUES (?, ?, ?, ?, ?, ?, (select MAX(id_orden_compra) from orden_compra), ?,?);";

            ps2 = con.prepareStatement(sql1);
            ps2.setInt(1, ord.getNro_orden_detalle());
            ps2.setString(2, ord.getDescrip_orden_det());
            ps2.setInt(3, ord.getCantidad_orden_det());
            ps2.setDouble(4, ord.getPrecio_unit_orden_det());
            ps2.setDouble(5, ord.getPrecio_total_orden_det());
            ps2.setDouble(6, ord.getCosto_total_orden_det());

            ps2.setInt(7, ord.getId_producto());
            ps2.setInt(8, ord.getId_aprobacion());

//CUANDO TENGA UN ARRAY DEBE INGRESAR EL PS2 EN UN WHILE
            ps2.executeUpdate();
//AQUI TERMINA EL WHILE
            return 1;
//
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return 0;
        }

    }

    public HashMap seleccionarOrdencompra() {
        HashMap<String, String> dropord_ord = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT id_orden_compra as id_ord, nro_orden_compra \n"
                    + "FROM public.orden_compra \n"
                    + "WHERE estado_orden_compra = 'activo';";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                dropord_ord.put(rs.getString("id_ord"), rs.getString("nro_orden_compra"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dropord_ord;
    }

    public Ordencompra buscarOrdencompra(String buscartxt) {
        int r = 0;
        Ordencompra ord = new Ordencompra();
        String sql = "SELECT ord.nro_orden_compra, ord.fecha_pedido_orden_compra,\n"
                + "ord.fecha_pago_orden_compra, ord.direccion_entrega_orden_compra, ord.estado_orden_compra,\n"
                + "ord.nombre_usuario_modificacion, ord.fecha_modificacion, ord.id_proveedor, ordet.descripcion_orden_compra_detalle,\n"
                + "ordet.cantidad_orden_compra_detalle, ordet.precio_unit_orden_compra_detalle, ordet.precio_total_orden_compra_detalle,\n"
                + "ordet.costo_total_orden_compra_detalle,ordet.id_producto, ordet.id_orden_compra, ordet.id_aprobacion_compra\n"
                + "FROM public.orden_compra ord\n"
                + "INNER JOIN proveedor prov\n"
                + "ON prov.id_proveedor = ord.id_proveedor\n"
                + "INNER JOIN orden_compra_detalle ordet\n"
                + "ON ord.id_orden_compra = ordet.id_orden_compra\n"
                + "INNER JOIN producto prod\n"
                + "ON ordet.id_producto = prod.id_producto\n"
                + "INNER JOIN aprobacion_compra apr\n"
                + "ON apr.id_aprobacion_compra = ordet.id_aprobacion_compra\n"
                + "WHERE ord.nro_orden_compra like ?;";

        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, buscartxt);

            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                ord.setNro_orden(rs.getString("nro_orden_compra"));
                ord.setFecha_pedido_orden(rs.getString("fecha_pedido_orden_compra"));
                ord.setFecha_pago_orden(rs.getString("fecha_pago_orden_compra"));
                ord.setDireccion_entrega_orden(rs.getString("direccion_entrega_orden_compra"));
                ord.setEstado_orden(rs.getString("estado_orden_compra"));
                ord.setNom_usu(rs.getString("nombre_usuario_modificacion"));
                ord.setId_proveedor(rs.getInt("id_proveedor"));

            }

        } catch (Exception e) {
            System.out.println("hola");

        } finally {
            return ord;
        }
    }
}
