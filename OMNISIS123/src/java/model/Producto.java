/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Alvaro
 */
public class Producto implements ValidarProducto {

    int id_producto;
    String descripcion_producto;
    String estado_producto;
    String nombre_usu_mod_producto;
    String descripcion_impuesto;
    int id_impuesto;

    public Producto() {
    }

    public Producto(int id_producto, String descripcion_producto, String estado_producto, String nombre_usu_mod_producto, String descripcion_impuesto, int id_impuesto) {
        this.id_producto = id_producto;
        this.descripcion_producto = descripcion_producto;
        this.estado_producto = estado_producto;
        this.nombre_usu_mod_producto = nombre_usu_mod_producto;
        this.descripcion_impuesto = descripcion_impuesto;
        this.id_impuesto = id_impuesto;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getDescripcion_producto() {
        return descripcion_producto;
    }

    public void setDescripcion_producto(String descripcion_producto) {
        this.descripcion_producto = descripcion_producto;
    }

    public String getEstado_producto() {
        return estado_producto;
    }

    public void setEstado_producto(String estado_producto) {
        this.estado_producto = estado_producto;
    }

    public String getNombre_usu_mod_producto() {
        return nombre_usu_mod_producto;
    }

    public void setNombre_usu_mod_producto(String nombre_usu_mod_producto) {
        this.nombre_usu_mod_producto = nombre_usu_mod_producto;
    }

    public String getDescripcion_impuesto() {
        return descripcion_impuesto;
    }

    public void setDescripcion_impuesto(String descripcion_impuesto) {
        this.descripcion_impuesto = descripcion_impuesto;
    }

    public int getId_impuesto() {
        return id_impuesto;
    }

    public void setId_impuesto(int id_impuesto) {
        this.id_impuesto = id_impuesto;
    }

    //FUNCIONES REGISTRAR - BUSCAR - MODIFICAR - CAMBIAR ESTADO
    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;

    @Override
    public int registrarProducto(Producto pr) {
        int r = 0;
        String sql = "INSERT INTO public.producto (descripcion_producto, estado_producto, nombre_usuario_modificacion, fecha_modificacion, id_impuesto)\n"
                + "	VALUES (?,?,?,(SELECT current_date),?)";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, pr.getDescripcion_producto());
            ps.setString(2, pr.getEstado_producto());
            ps.setString(3, pr.getNombre_usu_mod_producto());
            ps.setInt(4, pr.getId_impuesto());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                // Variables.id = Integer.parseInt(rs.getString("id_usuario"));
                pr.setDescripcion_producto(rs.getString("descripcion_producto"));
                pr.setEstado_producto(rs.getString("estado_producto"));
                pr.setNombre_usu_mod_producto(rs.getString("nombre_usuario_modificacion"));
                pr.setId_impuesto(rs.getInt("id_impuesto"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }

        } catch (Exception e) {
            return 0;
        }
    }

    public Producto buscarProducto(String buscartxt) {
        int r = 0;
        Producto pr = new Producto();
        String sql = "SELECT pro.id_producto, pro.descripcion_producto, imp.descripcion_impuesto\n"
                + "FROM producto pro\n"
                + "INNER JOIN impuesto imp\n"
                + "ON pro.id_impuesto = imp.id_impuesto\n"
                + "WHERE pro.descripcion_producto ILIKE ?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, buscartxt);

            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                pr.setDescripcion_producto(rs.getString("descripcion_producto"));
                pr.setDescripcion_impuesto(rs.getString("descripcion_impuesto"));
                pr.setId_producto(rs.getInt("id_producto"));
            }

        } catch (Exception e) {
            System.out.println("hola");

        } finally {
            return pr;
        }
    }

    public HashMap seleccionarProducto() {
        HashMap<String, String> droppro_pro = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT id_producto as id_pro,descripcion_producto\n"
                    + "FROM public.producto\n"
                    + "WHERE estado_producto = 'activo';";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                droppro_pro.put(rs.getString("id_pro"), rs.getString("descripcion_producto"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return droppro_pro;
    }

}
