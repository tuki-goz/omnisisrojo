/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Alvaro
 */
public class Variables {
    
    public static int id;
    public static int contadorfallos = 0;
    public static String usuario1;
    public static int codigoDetalle;
    public static int codigoCompra;
    public static String fallo;
    public static int id_perfil;
    public static int ban = 0;
    public static int ban1 = 0;
    public static int ban2 = 0;
    public static int ban3 = 0;
    public static int ocultar = 0;
    public static Double tota_compra=0.0;
    public static Double iva5=0.0;
    public static Double iva10=0.0;
    public static Double exenta=0.0;
    public static Double sub_total=0.0;
    public static Double totalliq5=0.0;
    public static Double totalliq10=0.0;
    public static Double totalliqui=0.0;
    
    public static ArrayList <Pedido> pedidoDet1 = new ArrayList<>();
    public static ArrayList<Pedido> getPedidos(){
        return pedidoDet1;
    }
    public static void addPedidos(Pedido pedido) {
        Variables.pedidoDet1.add(pedido);
    }
    public static ArrayList <Compra> compraDet1 = new ArrayList<>();
    public static ArrayList<Compra> getCompras(){
        return compraDet1;
    }
    public static void addCompras(Compra com) {
        Variables.compraDet1.add(com);
    }
    public static ArrayList <Compra> compraDet2 = new ArrayList<>();
    public static ArrayList<Compra> getCompras1(){
        return compraDet2;
    }
    public static void addCompras1(Compra com) {
        Variables.compraDet2.add(com);
    }
     public static ArrayList <Compra> compraDet3 = new ArrayList<>();
    public static ArrayList<Compra> getCompras2(){
        return compraDet3;
    }
    public static void addCompras2(Compra com) {
        Variables.compraDet3.add(com);
    }
    public static Usuario usuarioConectado = new Usuario(); //variable creada para almacenar todos los datos de usuario
}
