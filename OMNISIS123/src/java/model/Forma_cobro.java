/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Francisca Gómez
 */
public class Forma_cobro implements ValidarFormaCobro {

    //CONSTRUCTORES - SET - GET
    int id_forma_cobro;
    String descrip_forma_cobro;

    public Forma_cobro() {
    }

    public Forma_cobro(int id_forma_cobro, String descrip_forma_cobro) {
        this.id_forma_cobro = id_forma_cobro;
        this.descrip_forma_cobro = descrip_forma_cobro;
    }

    public int getId_forma_cobro() {
        return id_forma_cobro;
    }

    public void setId_forma_cobro(int id_forma_cobro) {
        this.id_forma_cobro = id_forma_cobro;
    }

    public String getDescrip_forma_cobro() {
        return descrip_forma_cobro;
    }

    public void setDescrip_forma_cobro(String descrip_forma_cobro) {
        this.descrip_forma_cobro = descrip_forma_cobro;
    }

    //FUNCIONES REGISTRAR - BUSCAR - MODIFICAR - CAMBIAR ESTADO
    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;

    @Override
    public int registrarFormaCobro(Forma_cobro fc) {
        int r = 0;
        String sql = "INSERT INTO public.forma_cobro(\n"
                + "descripcion_forma_cobro)\n"
                + "VALUES (?);";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, fc.getDescrip_forma_cobro());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                Variables.id = Integer.parseInt(rs.getString("id_usuario"));
                fc.setDescrip_forma_cobro(rs.getString("descripcion_forma_cobro"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public HashMap seleccionarFormaCobro() {
        HashMap<String, String> drop_fc = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT id_forma_cobro as id_fc,descripcion_forma_cobro\n"
                    + "FROM public.forma_cobro;";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                drop_fc.put(rs.getString("id_fc"), rs.getString("descripcion_forma_cobro"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return drop_fc;
    }

    public Forma_cobro buscarFormaCobro(String buscartxt) {
        int r = 0;
        Forma_cobro fc = new Forma_cobro();
        String sql = "SELECT id_forma_cobro, descripcion_forma_cobro\n"
                + "FROM forma_cobro\n"
                + "WHERE descripcion_forma_cobro = ?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, buscartxt);
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                fc.setId_forma_cobro(rs.getInt("id_forma_cobro"));
                fc.setDescrip_forma_cobro(rs.getString("descripcion_forma_cobro"));
            }
        } catch (Exception e) {
            System.out.println("hola");
        } finally {
            return fc;
        }
    }

    public int modificarFormaCobro(Forma_cobro fc) {
        int r = 0;
        String sql = "UPDATE public.forma_cobro\n"
                + "SET descripcion_forma_cobro=?\n"
                + "WHERE id_forma_cobro=?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, fc.getDescrip_forma_cobro());
            ps.setInt(2, fc.getId_forma_cobro());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                fc.setDescrip_forma_cobro(rs.getString("descripcion_forma_cobro"));                
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }
}
