/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Francisca Gómez
 */
public class Pedido {

    int codigo, id_pedido, id_usuario, id_servicio, id_pedido_detm, cant_det, id_prod;
    String fecha, nro_pedido, estado, descrip_det;
    private Connection conn;
    private Statement stm;
    private ResultSet rs;
    private Pedido pedidoDetHallado;

    public Pedido(int codigo, int id_pedido, int id_usuario, int id_servicio, String fecha, String nro_pedido, String estado) {
        this.codigo = codigo;
        this.id_pedido = id_pedido;
        this.id_usuario = id_usuario;
        this.id_servicio = id_servicio;
        this.fecha = fecha;
        this.nro_pedido = nro_pedido;
        this.estado = estado;
    }

    public Pedido(int id_pedido_detm, String descrip_det, int cant_det, int id_prod, int id_pedido) {
        this.id_pedido = id_pedido;
        this.id_pedido_detm = id_pedido_detm;
        this.cant_det = cant_det;
        this.id_prod = id_prod;
        this.descrip_det = descrip_det;
    }

    public Pedido() {
    }

    public Pedido(int cant_det, int id_prod, String descrip_det) {
        this.cant_det = cant_det;
        this.id_prod = id_prod;
        this.descrip_det = descrip_det;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_servicio() {
        return id_servicio;
    }

    public void setId_servicio(int id_servicio) {
        this.id_servicio = id_servicio;
    }

    public int getId_pedido_detm() {
        return id_pedido_detm;
    }

    public void setId_pedido_detm(int id_pedido_detm) {
        this.id_pedido_detm = id_pedido_detm;
    }

    public int getCant_det() {
        return cant_det;
    }

    public void setCant_det(int cant_det) {
        this.cant_det = cant_det;
    }

    public int getId_prod() {
        return id_prod;
    }

    public void setId_prod(int id_prod) {
        this.id_prod = id_prod;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNro_pedido() {
        return nro_pedido;
    }

    public void setNro_pedido(String nro_pedido) {
        this.nro_pedido = nro_pedido;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescrip_det() {
        return descrip_det;
    }

    public void setDescrip_det(String descrip_det) {
        this.descrip_det = descrip_det;
    }

    public ArrayList<Pedido> leerPedidoDet() {
        ArrayList<Pedido> pedidoDet = new ArrayList<Pedido>();
        try {
            conn = ConectaBD.abrir();
            stm = conn.createStatement();
            rs = stm.executeQuery("SELECT id_pedido_detalle, descripcion_pedido_parte_detalle, cantidad_pedido_producto_detalle, id_producto, id_pedido\n"
                    + "	FROM public.pedido_detalle;");
            if (!rs.next()) {
                System.out.println("No se encontro el registro");
                ConectaBD.cerrar();
                return pedidoDet;
            } else {
                do {
                    id_pedido_detm = rs.getInt("id_pedido_detalle");
                    descrip_det = rs.getString("descripcion_pedido_parte_detalle");
                    cant_det = rs.getInt("cantidad_pedido_producto_detalle");
                    id_prod = rs.getInt("id_producto");
                    id_pedido = rs.getInt("id_pedido");
                    pedidoDetHallado = new Pedido(id_pedido_detm, descrip_det, cant_det, id_prod, id_pedido);
                    pedidoDet.add(pedidoDetHallado);
                } while (rs.next());
                ConectaBD.cerrar();
                return pedidoDet;
            }

        } catch (Exception e) {
            System.out.println("Error en la base de datos");
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Pedido> leerPedido() {
        ArrayList<Pedido> pedidoDet = new ArrayList<Pedido>();
        return pedidoDet;           
    }

}
