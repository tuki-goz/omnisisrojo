/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Alvaro
 */
public class Compra implements ValidarCompra {

    //CONSTRUCTORES - SET - GET
    int id_compra, id_proveedor, id_contrato, id_moneda, id_usuario, id_compra_Det, cantidad_Det, id_orden_compra, cod, id_tipo_p, id_forma_c, id_estado;
    String codigo, proveedor, fecha_compra, estado_compra, nom_usu_mod, fecha_mod, descripcion_Det,
            descrip_prov, cod_contrato, descrip_moneda, descrip_orden, nombre_usuario, descrip_tipo_pago, descrip_forma_cobro, descrip_estado, nro_contrato;
    Double total_compra, iva5_Det, iva10_Det, exenta_Det, precio_Det, sub_total, liq5, liq10, totaliva;

    public Compra() {
    }

    public Compra(int id_compra, int id_proveedor, int id_contrato, int id_moneda, int id_usuario, String codigo, String proveedor, String fecha_compra, String estado_compra, String nom_usu_mod, String fecha_mod, Double total_compra) {
        this.id_compra = id_compra;
        this.id_proveedor = id_proveedor;
        this.id_contrato = id_contrato;
        this.id_moneda = id_moneda;
        this.id_usuario = id_usuario;
        this.codigo = codigo;
        this.proveedor = proveedor;
        this.fecha_compra = fecha_compra;
        this.estado_compra = estado_compra;
        this.nom_usu_mod = nom_usu_mod;
        this.fecha_mod = fecha_mod;
        this.total_compra = total_compra;
    }

    public Compra(int id_compra, int id_compra_Det, int cantidad_Det, int id_orden_compra, String descripcion_Det, Double iva5_Det, Double iva10_Det, Double exenta_Det, Double precio_Det) {
        this.id_compra = id_compra;
        this.id_compra_Det = id_compra_Det;
        this.cantidad_Det = cantidad_Det;
        this.id_orden_compra = id_orden_compra;
        this.descripcion_Det = descripcion_Det;
        this.iva5_Det = iva5_Det;
        this.iva10_Det = iva10_Det;
        this.exenta_Det = exenta_Det;
        this.precio_Det = precio_Det;
    }

    public Compra(Double total_compra, Double sub_total) {
        this.total_compra = total_compra;
        this.sub_total = sub_total;
    }

    public Compra(Double liq5, Double liq10, Double totaliva) {
        this.liq5 = liq5;
        this.liq10 = liq10;
        this.totaliva = totaliva;
    }

    public Compra(int id_compra, String codigo, String descrip_estado, String nro_contrato) {
        this.id_compra = id_compra;
        this.codigo = codigo;
        this.descrip_estado = descrip_estado;
        this.nro_contrato = nro_contrato;
    }

    public int getId_tipo_p() {
        return id_tipo_p;
    }

    public void setId_tipo_p(int id_tipo_p) {
        this.id_tipo_p = id_tipo_p;
    }

    public int getId_forma_c() {
        return id_forma_c;
    }

    public void setId_forma_c(int id_forma_c) {
        this.id_forma_c = id_forma_c;
    }

    public int getId_estado() {
        return id_estado;
    }

    public void setId_estado(int id_estado) {
        this.id_estado = id_estado;
    }

    public String getNro_contrato() {
        return nro_contrato;
    }

    public void setNro_contrato(String nro_contrato) {
        this.nro_contrato = nro_contrato;
    }

    public Double getTotaliva() {
        return totaliva;
    }

    public void setTotaliva(Double totaliva) {
        this.totaliva = totaliva;
    }

    public Double getLiq5() {
        return liq5;
    }

    public void setLiq5(Double liq5) {
        this.liq5 = liq5;
    }

    public Double getLiq10() {
        return liq10;
    }

    public void setLiq10(Double liq10) {
        this.liq10 = liq10;
    }

    public Double getSub_total() {
        return sub_total;
    }

    public void setSub_total(Double sub_total) {
        this.sub_total = sub_total;
    }

    public String getDescrip_tipo_pago() {
        return descrip_tipo_pago;
    }

    public void setDescrip_tipo_pago(String descrip_tipo_pago) {
        this.descrip_tipo_pago = descrip_tipo_pago;
    }

    public String getDescrip_forma_cobro() {
        return descrip_forma_cobro;
    }

    public void setDescrip_forma_cobro(String descrip_forma_cobro) {
        this.descrip_forma_cobro = descrip_forma_cobro;
    }

    public String getDescrip_estado() {
        return descrip_estado;
    }

    public void setDescrip_estado(String descrip_estado) {
        this.descrip_estado = descrip_estado;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public String getDescrip_orden() {
        return descrip_orden;
    }

    public void setDescrip_orden(String descrip_orden) {
        this.descrip_orden = descrip_orden;
    }

    public String getDescrip_moneda() {
        return descrip_moneda;
    }

    public void setDescrip_moneda(String descrip_moneda) {
        this.descrip_moneda = descrip_moneda;
    }

    public String getCod_contrato() {
        return cod_contrato;
    }

    public void setCod_contrato(String cod_contrato) {
        this.cod_contrato = cod_contrato;
    }

    public String getDescrip_prov() {
        return descrip_prov;
    }

    public void setDescrip_prov(String descrip_prov) {
        this.descrip_prov = descrip_prov;
    }

    public int getId_compra() {
        return id_compra;
    }

    public void setId_compra(int id_compra) {
        this.id_compra = id_compra;
    }

    public int getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(int id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public int getId_contrato() {
        return id_contrato;
    }

    public void setId_contrato(int id_contrato) {
        this.id_contrato = id_contrato;
    }

    public int getId_moneda() {
        return id_moneda;
    }

    public void setId_moneda(int id_moneda) {
        this.id_moneda = id_moneda;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_compra_Det() {
        return id_compra_Det;
    }

    public void setId_compra_Det(int id_compra_Det) {
        this.id_compra_Det = id_compra_Det;
    }

    public int getCantidad_Det() {
        return cantidad_Det;
    }

    public void setCantidad_Det(int cantidad_Det) {
        this.cantidad_Det = cantidad_Det;
    }

    public int getId_orden_compra() {
        return id_orden_compra;
    }

    public void setId_orden_compra(int id_orden_compra) {
        this.id_orden_compra = id_orden_compra;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getFecha_compra() {
        return fecha_compra;
    }

    public void setFecha_compra(String fecha_compra) {
        this.fecha_compra = fecha_compra;
    }

    public String getEstado_compra() {
        return estado_compra;
    }

    public void setEstado_compra(String estado_compra) {
        this.estado_compra = estado_compra;
    }

    public String getNom_usu_mod() {
        return nom_usu_mod;
    }

    public void setNom_usu_mod(String nom_usu_mod) {
        this.nom_usu_mod = nom_usu_mod;
    }

    public String getFecha_mod() {
        return fecha_mod;
    }

    public void setFecha_mod(String fecha_mod) {
        this.fecha_mod = fecha_mod;
    }

    public String getDescripcion_Det() {
        return descripcion_Det;
    }

    public void setDescripcion_Det(String descripcion_Det) {
        this.descripcion_Det = descripcion_Det;
    }

    public Double getTotal_compra() {
        return total_compra;
    }

    public void setTotal_compra(Double total_compra) {
        this.total_compra = total_compra;
    }

    public Double getIva5_Det() {
        return iva5_Det;
    }

    public void setIva5_Det(Double iva5_Det) {
        this.iva5_Det = iva5_Det;
    }

    public Double getIva10_Det() {
        return iva10_Det;
    }

    public void setIva10_Det(Double iva10_Det) {
        this.iva10_Det = iva10_Det;
    }

    public Double getExenta_Det() {
        return exenta_Det;
    }

    public void setExenta_Det(Double exenta_Det) {
        this.exenta_Det = exenta_Det;
    }

    public Double getPrecio_Det() {
        return precio_Det;
    }

    public void setPrecio_Det(Double precio_Det) {
        this.precio_Det = precio_Det;
    }

//FUNCIONES REGISTRAR - BUSCAR - MODIFICAR - CAMBIAR ESTADO
    //CAMBSOAS
    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    PreparedStatement ps2;
    PreparedStatement ps3;
    PreparedStatement ps4;
    ResultSet rs;
    ResultSet rs2;
    ResultSet rs3;
    private Statement stm;
    private Compra compraHallado;

    @Override
    public int registrarCompra(Compra com) {
        try {
            con = cn.getConnection();
            String sql = "INSERT INTO public.compra(\n"
                    + "codigo_compra, fecha_compra, total_compra, subtotal,\n"
                    + "nombre_usuario_modificacion, fecha_modificacion, id_proveedor,\n"
                    + "id_moneda, id_usuario, id_tipo_pago, id_forma_cobro, id_estado, nro_contrato, liqiva5, liqiva10, totalliq)\n"
                    + "VALUES (?, ?, ?, ?, ?, (SELECT current_date), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            ps = con.prepareStatement(sql);
            ps.setString(1, com.getCodigo());
            ps.setString(2, com.getFecha_compra());
            ps.setDouble(3, com.getTotal_compra());
            ps.setDouble(4, com.getSub_total());
            ps.setString(5, com.getNom_usu_mod());
            ps.setInt(6, com.getId_proveedor());
            ps.setInt(7, com.getId_moneda());
            ps.setInt(8, com.getId_usuario());
            ps.setInt(9, com.getId_tipo_p());
            ps.setInt(10, com.getId_forma_c());
            ps.setInt(11, com.getId_estado());
            ps.setString(12, com.getNro_contrato());
            ps.setDouble(13, com.getLiq5());
            ps.setDouble(14, com.getLiq10());
            ps.setDouble(15, com.getTotaliva());
            ps.executeUpdate();

            // insert detalle
            String sql1 = "INSERT INTO public.compra_detalle(\n"
                    + "	cantidad_compra_d, descripcion, iva5_compra_d, iva10_compra_d, \n"
                    + "	excentas_compra_d, precio_compra_d, id_orden_compra, id_compra)\n"
                    + "	VALUES (?, ?, ?, ?, ?, ?, ?, (select MAX(id_compra) from compra));";

            for (Compra comdet : Variables.compraDet1) {
                ps2 = con.prepareStatement(sql1);

                ps2.setInt(1, comdet.getCantidad_Det());
                ps2.setString(2, comdet.getDescripcion_Det());
                ps2.setDouble(3, comdet.getIva5_Det());
                ps2.setDouble(4, comdet.getIva10_Det());
                ps2.setDouble(5, comdet.getExenta_Det());
                ps2.setDouble(6, comdet.getPrecio_Det());
                ps2.setInt(7, comdet.getId_orden_compra());
                ps2.executeUpdate();
            }
            Variables.compraDet1.clear();
            return 1;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return 0;
        }
    }

    public HashMap seleccionarCompra() {
        HashMap<String, String> dropcompra_det = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT com.id_compra as id_com,comet.descripcion\n"
                    + "FROM compra com\n"
                    + "INNER JOIN compra_detalle comet\n"
                    + "ON comet.id_compra = com.id_compra\n"
                    + "WHERE estado_compra = 'activo'";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                dropcompra_det.put(rs.getString("id_com"), rs.getString("comet.descripcion"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dropcompra_det;
    }

    public Compra buscarCompra(String buscartxt) {
        int r = 0;
        Compra com = new Compra();
        String sql = "SELECT com.id_compra, com.codigo_compra, com.fecha_compra,\n"
                + "prov.nombre_proveedor, mon.descripcion_moneda, usu.nombre_usuario, tp.descripcion_tipo_pago, fc.descripcion_forma_cobro,\n"
                + "est.descrip_estado, com.nro_contrato\n"
                + "FROM  compra com\n"
                + "INNER JOIN proveedor prov\n"
                + "ON com.id_proveedor = prov.id_proveedor\n"
                + "INNER JOIN moneda mon\n"
                + "ON com.id_moneda = mon.id_moneda\n"
                + "INNER JOIN usuario usu\n"
                + "ON com.id_usuario = usu.id_usuario\n"
                + "INNER JOIN tipo_pago tp\n"
                + "USING(id_tipo_pago)\n"
                + "INNER JOIN forma_cobro fc\n"
                + "USING(id_forma_cobro)\n"
                + "INNER JOIN estado est\n"
                + "USING(id_estado)\n"
                + "WHERE com.id_compra = ?";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, Integer.valueOf(buscartxt));
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                com.setId_compra(rs.getInt("id_compra"));
                com.setCodigo(rs.getString("codigo_compra"));
                com.setFecha_compra(rs.getString("fecha_compra"));
                com.setDescrip_prov(rs.getString("nombre_proveedor"));
                com.setDescrip_moneda(rs.getString("descripcion_moneda"));
                com.setNombre_usuario(rs.getString("nombre_usuario"));
                com.setDescrip_tipo_pago(rs.getString("descripcion_tipo_pago"));
                com.setDescrip_forma_cobro(rs.getString("descripcion_forma_cobro"));
                com.setDescrip_estado(rs.getString("descrip_estado"));
                com.setNro_contrato(rs.getString("nro_contrato"));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Variables.ban3 = 1;
        } finally {
            return com;
        }
    }

    public ArrayList<Compra> leerCompra() {
        ArrayList<Compra> compraDet = new ArrayList<>();
        return compraDet;
    }

    public ArrayList<Compra> leerCompraSub_Total() {
        ArrayList<Compra> compraTotal = new ArrayList<>();
        return compraTotal;
    }

    public ArrayList<Compra> leerCompraLiq() {
        ArrayList<Compra> compraLiqIva = new ArrayList<>();
        return compraLiqIva;
    }

    public ArrayList<Compra> leerDetalle(String cod) {
        ArrayList<Compra> compra = new ArrayList<>();//comentario para ayuda
        try {
            con = ConectaBD.abrir();
            stm = con.createStatement();
            rs = stm.executeQuery("SELECT id_compra_detalle, cantidad_compra_d, descripcion, iva5_compra_d, iva10_compra_d, excentas_compra_d, \n"
                    + "precio_compra_d, id_compra, id_orden_compra\n"
                    + "FROM public.compra_detalle\n"
                    + "WHERE id_compra = " + cod + "");
            if (!rs.next()) {
                System.out.println("No se encontro el registro");
                ConectaBD.cerrar();
                return null;
            } else {
                do {
                    id_compra = rs.getInt("id_compra");
                    id_compra_Det = rs.getInt("id_compra_detalle");
                    cantidad_Det = rs.getInt("cantidad_compra_d");
                    id_orden_compra = rs.getInt("id_orden_compra");
                    descripcion_Det = rs.getString("descripcion");
                    iva5_Det = rs.getDouble("iva5_compra_d");
                    iva10_Det = rs.getDouble("iva10_compra_d");
                    exenta_Det = rs.getDouble("excentas_compra_d");
                    precio_Det = rs.getDouble("precio_compra_d");
                    compraHallado = new Compra(id_compra, id_compra_Det, cantidad_Det, id_orden_compra, descripcion_Det, iva5_Det, iva10_Det, exenta_Det, precio_Det);
                    compra.add(compraHallado);
                } while (rs.next());
                ConectaBD.cerrar();
                return compra;
            }

        } catch (Exception e) {
            System.out.println("Error en la base de datos");
            e.printStackTrace();
            return null;
        }
    }

    public Compra leerUnDetalle(String cod) {

        try {
            con = ConectaBD.abrir();
            stm = con.createStatement();
            rs = stm.executeQuery("SELECT id_compra_detalle, cantidad_compra_d, descripcion, iva5_compra_d, iva10_compra_d, excentas_compra_d, \n"
                    + "precio_compra_d, id_compra, id_orden_compra\n"
                    + "FROM public.compra_detalle\n"
                    + "WHERE id_compra_detalle = " + cod + "");
            if (!rs.next()) {
                System.out.println("No se encontro el registro");
                ConectaBD.cerrar();
                return null;
            } else {
                do {

                    id_compra = rs.getInt("id_compra");
                    id_compra_Det = rs.getInt("id_compra_detalle");
                    cantidad_Det = rs.getInt("cantidad_compra_d");
                    id_orden_compra = rs.getInt("id_orden_compra");
                    descripcion_Det = rs.getString("descripcion");
                    iva5_Det = rs.getDouble("iva5_compra_d");
                    iva10_Det = rs.getDouble("iva10_compra_d");
                    exenta_Det = rs.getDouble("excentas_compra_d");
                    precio_Det = rs.getDouble("precio_compra_d");
                    compraHallado = new Compra(id_compra, id_compra_Det, cantidad_Det, id_orden_compra, descripcion_Det, iva5_Det, iva10_Det, exenta_Det, precio_Det);
                } while (rs.next());
                ConectaBD.cerrar();
                return compraHallado;
            }

        } catch (Exception e) {
            System.out.println("Error en la base de datos");
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Compra> leerTotal(String cod) {
        ArrayList<Compra> compra = new ArrayList<>();
        try {
            con = ConectaBD.abrir();
            stm = con.createStatement();
            rs = stm.executeQuery("SELECT total_compra, subtotal\n"
                    + "FROM public.compra\n"
                    + "WHERE id_compra = " + cod + "");
            if (!rs.next()) {
                System.out.println("No se encontro el registro");
                ConectaBD.cerrar();
                return null;
            } else {
                do {
                    Variables.tota_compra = (rs.getDouble("total_compra"));
                    Variables.sub_total = (rs.getDouble("subtotal"));

                    compraHallado = new Compra(Variables.tota_compra, Variables.sub_total);
                    compra.add(compraHallado);
                } while (rs.next());
                ConectaBD.cerrar();
                return compra;
            }

        } catch (Exception e) {
            System.out.println("Error en la base de datos");
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Compra> leerLiquidacion(String cod) {
        ArrayList<Compra> compra = new ArrayList<>();
        try {
            con = ConectaBD.abrir();
            stm = con.createStatement();
            rs = stm.executeQuery("SELECT liqiva5, liqiva10, totalliq\n"
                    + "FROM public.compra\n"
                    + "WHERE id_compra = " + cod + "");
            if (!rs.next()) {
                System.out.println("No se encontro el registro");
                ConectaBD.cerrar();
                return null;
            } else {
                do {
                    Variables.totalliq5 = rs.getDouble("liqiva5");
                    Variables.totalliq10 = rs.getDouble("liqiva10");
                    Variables.totalliqui = rs.getDouble("totalliq");

                    compraHallado = new Compra(Variables.totalliq5, Variables.totalliq10, Variables.totalliqui);
                    compra.add(compraHallado);
                } while (rs.next());
                ConectaBD.cerrar();
                return compra;
            }

        } catch (Exception e) {
            System.out.println("Error en la base de datos");
            e.printStackTrace();
            return null;
        }
    }

    public int modificarCompra(Compra com) {
        int r = 0;
        String sql = "UPDATE public.compra\n"
                + "	SET codigo_compra=?, fecha_compra=?, total_compra=?, nombre_usuario_modificacion=?, fecha_modificacion=(SELECT current_date), \n"
                + "	id_proveedor=?, id_moneda=?, id_usuario=?, id_tipo_pago=?, id_forma_cobro=?,\n"
                + "	id_estado=?, nro_contrato=?, subtotal=?, liqiva5=?, liqiva10=?, totalliq=?\n"
                + "	WHERE id_compra=?";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, com.getCodigo());
            ps.setString(2, com.getFecha_compra());
            ps.setDouble(3, com.getTotal_compra());
            ps.setString(4, com.getNom_usu_mod());
            ps.setInt(5, com.getId_proveedor());
            ps.setInt(6, com.getId_moneda());
            ps.setInt(7, com.getId_usuario());
            ps.setInt(8, com.getId_tipo_p());
            ps.setInt(9, com.getId_forma_c());
            ps.setInt(10, com.getId_estado());
            ps.setString(11, com.getNro_contrato());
            ps.setDouble(12, com.getSub_total());
            ps.setDouble(13, com.getLiq5());
            ps.setDouble(14, com.getLiq10());
            ps.setDouble(15, com.getTotaliva());
            ps.setInt(16, com.getId_compra());

            ps.executeUpdate();

            return 1;

        } catch (Exception e) {
            return 0;
        }
    }

    public int modificarDetalle(Compra com) {
        int r = 0;
        String sql = "UPDATE public.compra_detalle\n"
                + "	SET cantidad_compra_d=?, descripcion=?, iva5_compra_d=?, iva10_compra_d=?, excentas_compra_d=?, precio_compra_d=?, id_orden_compra=?\n"
                + "	WHERE id_compra_detalle=?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, com.getCantidad_Det());
            ps.setString(2, com.getDescripcion_Det());
            ps.setDouble(3, com.getIva5_Det());
            ps.setDouble(4, com.getIva10_Det());
            ps.setDouble(5, com.getExenta_Det());
            ps.setDouble(6, com.getPrecio_Det());
            ps.setInt(7, com.getId_orden_compra());
            ps.setInt(8, com.getId_compra_Det());
            ps.executeUpdate();

            return 1;

        } catch (Exception e) {
            return 0;
        }
    }

    public int modificarSub(Compra com) {
        int r = 0;
        String sql = "UPDATE public.compra\n"
                + "SET total_compra=?, subtotal=?\n"
                + "WHERE id_compra=?";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setDouble(1, com.getTotal_compra());
            ps.setDouble(2, com.getSub_total());
            ps.setInt(3, com.getId_compra());
            ps.executeUpdate();

            return 1;

        } catch (Exception e) {
            return 0;
        }
    }

    public int modificarLiquidacion(Compra com) {
        int r = 0;
        String sql = "UPDATE public.compra\n"
                + "SET liqiva5=?, liqiva10=?, totalliq=?\n"
                + "WHERE id_compra=?";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setDouble(1, com.getLiq5());
            ps.setDouble(2, com.getLiq10());
            ps.setDouble(3, com.getTotaliva());
            ps.setInt(4, com.getId_compra());
            ps.executeUpdate();

            return 1;

        } catch (Exception e) {
            return 0;
        }
    }

    public ArrayList<Compra> leerCompraTo() {
        ArrayList<Compra> comp = new ArrayList<>();
        try {
            con = ConectaBD.abrir();
            stm = con.createStatement();
            rs = stm.executeQuery("SELECT id_compra, codigo_compra, nro_contrato, es.descrip_estado\n"
                    + "FROM public.compra\n"
                    + "INNER JOIN estado es\n"
                    + "USING(id_estado)\n"
                    + "ORDER BY id_compra");
            if (!rs.next()) {
                System.out.println("No se encontro el registro");
                ConectaBD.cerrar();
                return null;
            } else {
                do {
                    id_compra = rs.getInt("id_compra");
                    codigo = rs.getString("codigo_compra");
                    nro_contrato = rs.getString("nro_contrato");
                    descrip_estado = rs.getString("descrip_estado");
                    compraHallado = new Compra(id_compra, codigo, nro_contrato, descrip_estado);
                    comp.add(compraHallado);
                } while (rs.next());
                ConectaBD.cerrar();
                return comp;
            }
        } catch (Exception e) {
            System.out.println("Error en la base de datos");
            e.printStackTrace();
            return null;
        }
    }
}
